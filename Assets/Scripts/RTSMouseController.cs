﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSMouseController : MonoBehaviour
{
    Vehicle selectedVehicle;

    Ray ray;
    RaycastHit hit;
    Camera mainCamera;
    int mask = 1 << 8 | 1 << 12;
    public GameObject movePointer;

    void Awake()
    {
        movePointer = Instantiate(movePointer);
        movePointer.SetActive(false);
    }

	void Start ()
    {
        mainCamera = FindObjectOfType<Camera>();
	}

	void Update ()
    {
        if(Input.GetMouseButtonDown(0))
        {
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                //Debug.Log(Vector3.Angle(mainCamera.transform.position,hit.normal));
                //Debug.DrawRay(hit.transform.position, hit.normal,Color.red,Mathf.Infinity);

                if (hit.transform.parent && hit.transform.parent.GetComponent<Vehicle>() && hit.transform.parent.GetComponent<Vehicle>().isTeam)
                {
                    selectedVehicle = hit.transform.parent.GetComponent<Vehicle>();
                    selectedVehicle.Select(true);
                }
                else if (selectedVehicle && hit.transform.gameObject.layer == 12)
                {
                    if(!Input.GetKey(KeyCode.LeftShift))
                    {
                        selectedVehicle.commands.Clear();
                    }
                    Command cmd = new Command();
                    cmd.position = hit.point;
                    selectedVehicle.commands.Add(cmd);
                    movePointer.SetActive(true);
                    movePointer.transform.position = new Vector3(hit.point.x, 2, hit.point.z);
                }
            }
        }
        if(Input.GetMouseButtonDown(1))
        {
            if(selectedVehicle)
            {
                selectedVehicle.Select(false);
                selectedVehicle = null;
            }
        }
    }
}
