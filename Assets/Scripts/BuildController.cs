﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class BuildController : MonoBehaviour
{
    public GameObject cube;
    public GameObject vehicleObj;
    public GameObject loadShipButton,selectBlockButton;
    public GameObject mirrorObj;
    public GameObject loadMenuBGPrefab;
    public GameObject buildMatBG,buildMatButton;

    GameObject loadMenuBG, loadMenuContent, messageBG;
    Text messageText,dimensionText;
    Vehicle vehicle;
    GameObject tBlock;
    InputField shipNameInputField;
    Camera mainCamera;
    BlockDictionary blockDict;
    RaycastHit hit;
    Ray ray;
    CameraFollow cameraScript;
    GameObject buildBG, debugBlocksBG, metalBlocksBG, functionalBlocksBG, turretEditorBG, roomEditorBG,woodBlocksBG;
    int currentBlockVal = 0;
    int placedMask = 1 << 8;
    List<GameObject> loadMenuButtonList = new List<GameObject>();
    List<GameObject> buildMenuList = new List<GameObject>();
    GameObject place;
    GameObject placeOne, placeTwo;
    Vector3 relativeUp, relativeRight, relativeForward, objectRelativeUp, objectRelativeRight, objectRelativeForward;
    bool isMirrored;
    bool buildMode = true;
    Vector3 size;

    Turret editTurret;
    Text turretStatsText;
    Toggle turretAntiAirToggle;
    InputField turretName, barrelSliderText, caliberSliderText, barrelLengthSliderText;
    Slider barrelSlider, caliberSlider, barrelLengthSlider;

    Rooms editRoom;
    Dropdown roomEditorDropdown;
    Text roomCountText, roomStatsText;

    Turret clipboard;

    void Awake()
    {
        loadMenuBG = Instantiate(loadMenuBGPrefab);
        loadMenuBG.transform.SetParent(transform.parent);
        loadMenuBG.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        vehicle = vehicleObj.GetComponent<Vehicle>();
        mainCamera = FindObjectOfType<Camera>();
        blockDict = FindObjectOfType<BlockDictionary>();
        cameraScript = FindObjectOfType<CameraFollow>();
        loadMenuContent = GameObject.Find("LoadMenuContent");
        shipNameInputField = GameObject.Find("ShipNameInputField").GetComponent<InputField>();
        buildBG = GameObject.Find("BuildBG");
        debugBlocksBG = GameObject.Find("DebugBlocksBG");
        metalBlocksBG = GameObject.Find("MetalBlocksBG");
        woodBlocksBG = GameObject.Find("WoodBlocksBG");
        functionalBlocksBG = GameObject.Find("FunctionalBlocksBG");
        messageBG = GameObject.Find("MessageBG");
        turretEditorBG = GameObject.Find("TurretEditorBG");
        roomEditorBG = GameObject.Find("RoomEditorBG");
        messageText = GameObject.Find("MessageText").GetComponent<Text>();
        dimensionText = GameObject.Find("DimensionText").GetComponent<Text>();
        GameObject.Find("BuildCanvas").GetComponent<CanvasScaler>().referenceResolution = new Vector2(Screen.width, Screen.height);

        barrelSliderText = GameObject.Find("BarrelSliderText").GetComponent<InputField>();
        caliberSliderText = GameObject.Find("CaliberSliderText").GetComponent<InputField>();
        barrelLengthSliderText = GameObject.Find("BarrelLengthSliderText").GetComponent<InputField>();
        turretStatsText = GameObject.Find("TurretStatsText").GetComponent<Text>();
        turretAntiAirToggle = GameObject.Find("TurretAntiAirToggle").GetComponent<Toggle>();
        turretName = GameObject.Find("TurretNameInputField").GetComponent<InputField>();
        barrelSlider = GameObject.Find("BarrelSlider").GetComponent<Slider>();
        caliberSlider = GameObject.Find("CaliberSlider").GetComponent<Slider>();
        barrelLengthSlider = GameObject.Find("BarrelLengthSlider").GetComponent<Slider>();

        roomEditorDropdown = GameObject.Find("RoomEditorDropdown").GetComponent<Dropdown>();
        roomCountText = GameObject.Find("RoomCountText").GetComponent<Text>();
        roomStatsText = GameObject.Find("RoomStatsText").GetComponent<Text>();

        barrelSlider.onValueChanged.AddListener(delegate { BarrelSliderChanged(); });
        caliberSlider.onValueChanged.AddListener(delegate { CaliberSliderChanged(); });
        barrelLengthSlider.onValueChanged.AddListener(delegate { BarrelLengthSliderChanged(); });
        turretName.onValueChanged.AddListener(delegate { TurretNameChanged(); });
        turretAntiAirToggle.onValueChanged.AddListener(delegate { OnToggleAntiAir(); });
        roomEditorDropdown.onValueChanged.AddListener(delegate { OnRoomDropdownChanged(); });

        barrelSliderText.onValueChanged.AddListener(delegate { BarrelTextChanged(); });
        caliberSliderText.onValueChanged.AddListener(delegate { CaliberTextChanged(); });
        barrelLengthSliderText.onValueChanged.AddListener(delegate { BarrelLengthTextChanged(); });
    }

    void Start()
    {
        //buildMenuList.Add(debugBlocksBG);
        //buildMenuList.Add(woodBlocksBG);
        //buildMenuList.Add(metalBlocksBG);
        //buildMenuList.Add(functionalBlocksBG);

        Timing.RunCoroutine(CreateMenuDelay());
        tBlock = Instantiate(cube);
        GameObject clone = Instantiate(blockDict.blockDict[0]);
        clone.transform.SetParent(vehicle.transform);
        clone.transform.localPosition = Vector3.zero;
        clone.transform.rotation = vehicle.transform.rotation;
        clone.transform.gameObject.layer = 8;
        clone.GetComponent<Block>().placeID = 0;
        clone.SetActive(true);
        foreach (Transform child in clone.transform)
        {
            child.gameObject.layer = 8;
        }
        vehicle.blocks.Add(clone.GetComponent<Block>());
        SelectBlock(0);
        messageBG.SetActive(false);

        vehicle.NewCenterOfMass();
        vehicle.SetTotalVolume();
    }

    IEnumerator<float> CreateMenuDelay()
    {
        yield return Timing.WaitForOneFrame;
        CreateBuildButtons();
        SwitchBuildMenu(0);
        mirrorObj.SetActive(false);
        buildBG.SetActive(false);
        loadMenuBG.SetActive(false);
        turretEditorBG.SetActive(false);
        roomEditorBG.SetActive(false);
    }

    bool isShowFunct = false;

    public void ShowFunctionalBlocks()
    {
        buildMode = false;

        for(int i=0;i<vehicle.blocks.Count;i++)
        {
            if (!vehicle.blocks[i].isFunctional)
            {
                for(int x=0;x<vehicle.blocks[i].blockChild.Count;x++)
                {
                    vehicle.blocks[i].gameObject.layer = 9;
                }
                vehicle.blocks[i].mesh.SetActive(false);
            }
        }
    }

    public void HideFunctionalBlocks()
    {
        buildMode = true;

        for(int i=0;i<vehicle.blocks.Count;i++)
        {
            for (int x = 0; x < vehicle.blocks[i].blockChild.Count; x++)
            {
                vehicle.blocks[i].gameObject.layer = 8;
            }
            vehicle.blocks[i].mesh.SetActive(true);
        }
    }

    bool OverlapCheck(Block block)
    {
        for(int i=0;i<block.blockChild.Count; i++)
        {
            if (Physics.OverlapBox(block.blockChild[i].transform.position, Vector3.one / 3, transform.rotation, placedMask).Length == 1)
            {
                return false;
            }
        }
        return true;
    }

    void OnRoomDropdownChanged()
    {
        editRoom.roomType = roomEditorDropdown.value;
    }

    void OnToggleAntiAir()
    {
        editTurret.isAA = turretAntiAirToggle.isOn;
    }

    void TurretNameChanged()
    {
        editTurret.turretName = turretName.text;
    }

    void BarrelSliderChanged()
    {
        barrelSliderText.text = barrelSlider.value.ToString();
        editTurret.barrelCount =  Convert.ToInt32(barrelSlider.value);
        editTurret.ChangeBarrels(editTurret.barrelCount);
    }

    void CaliberSliderChanged()
    {
        caliberSliderText.text = caliberSlider.value.ToString();
        editTurret.caliber = caliberSlider.value;
    }

    void BarrelLengthSliderChanged()
    {
        barrelLengthSliderText.text = barrelLengthSlider.value.ToString();
        editTurret.length = barrelLengthSlider.value;
        editTurret.ChangeBarrelLength(editTurret.length);
    }

    void BarrelTextChanged()
    {
        barrelSlider.value = int.Parse(barrelSliderText.text);
        editTurret.barrelCount = int.Parse(barrelSliderText.text);
        editTurret.ChangeBarrels(editTurret.barrelCount);
    }

    void CaliberTextChanged()
    {
        caliberSlider.value = float.Parse(caliberSliderText.text);
        editTurret.caliber = caliberSlider.value;
    }

    void BarrelLengthTextChanged()
    {
        barrelLengthSlider.value = float.Parse(barrelLengthSliderText.text);
        editTurret.length = barrelLengthSlider.value;
        editTurret.ChangeBarrelLength(editTurret.length);
    }

    void GetRoomData()
    {
        roomEditorDropdown.value = editRoom.roomType;
    }

    void GetTurretData()
    {
        barrelSlider.value = editTurret.barrelCount;
        caliberSlider.value = editTurret.caliber;
        barrelLengthSlider.value = editTurret.length;
        barrelSliderText.text = barrelSlider.value.ToString();
        caliberSliderText.text = caliberSlider.value.ToString();
        barrelLengthSliderText.text = barrelLengthSlider.value.ToString();
        turretAntiAirToggle.isOn = editTurret.isAA;
        turretName.text = editTurret.turretName;
    }

    public void CopyTurret()
    {
        clipboard = editTurret;
    }

    public void PasteTurret()
    {
        editTurret.turretName = clipboard.turretName;
        editTurret.barrelCount = clipboard.barrelCount;
        editTurret.caliber = clipboard.caliber;
        editTurret.length = clipboard.length;
        editTurret.isAA = clipboard.isAA;
        GetTurretData();
    }

    void CreateBuildButtons()
    {
        //Debug.Log(blockDict.blockDict[0].name);
        int offsetX = 0, offsetY=0;
        for (int x = 0; x < blockDict.materialList.Count; x++)
        {
            offsetX = 0;
            offsetY = 0;
            GameObject bg = Instantiate(buildMatBG, buildBG.transform);
            bg.GetComponent<RectTransform>().anchoredPosition = new Vector2(100.0f, 0.0f);
            buildMenuList.Add(bg);

            GameObject button = Instantiate(buildMatButton, buildBG.transform);
            button.GetComponent<RectTransform>().anchoredPosition = new Vector2(20.0f, -100f * x);
            button.GetComponent<BuildMatButton>().i = x;
            button.transform.GetChild(0).GetComponent<Text>().text = blockDict.materialList[x];
            for (int i = 0; i < blockDict.blockDict.Count; i++)
            {
                if (blockDict.blockDict[i].name.ToUpperInvariant().Contains(blockDict.materialList[x].ToUpperInvariant()))
                {
                    GameObject clone = Instantiate(selectBlockButton, bg.transform);
                    clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(offsetX * 120 + 75, offsetY * -100 - 75);
                    clone.GetComponent<BuildBlockButton>().blockID = i;
                    clone.GetComponent<BuildBlockButton>().bc = GetComponent<BuildController>();
                    clone.transform.GetChild(0).GetComponent<Text>().text = blockDict.blockDict[i].GetComponent<Block>().blockName;
                    offsetY++;
                    if (offsetY == 4)
                    {
                        offsetX++;
                        offsetY = 0;
                    }
                }
            }
        }
    }

    void Update()
    {
        if (shipNameInputField.isFocused)
        {
            mainCamera.GetComponent<CameraFollow>().locked = true; //optimize
            return;
        }
        if (Input.GetKeyDown(KeyCode.E) && !Input.GetKey(KeyCode.Tab))
        {
            buildBG.SetActive(!buildBG.activeInHierarchy);
            if (buildBG.activeInHierarchy)
            {
                Cursor.lockState = CursorLockMode.None;
                cameraScript.locked = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                cameraScript.locked = false;
            }
            if (turretEditorBG.activeInHierarchy)
            {
                turretEditorBG.SetActive(false);
                editTurret = null;
            }
            if(roomEditorBG.activeInHierarchy)
            {
                roomEditorBG.SetActive(false);
                editRoom = null;
            }
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            if(isShowFunct)
            {
                isShowFunct = false;
                HideFunctionalBlocks();
            }
            else
            {
                isShowFunct = true;
                ShowFunctionalBlocks();
            }
        }

        if (Input.GetKey(KeyCode.Tab))
        {
            relativeUp = mainCamera.transform.TransformDirection(Vector3.right);
            relativeRight = mainCamera.transform.TransformDirection(Vector3.up);
            relativeForward = mainCamera.transform.TransformDirection(Vector3.forward);
            objectRelativeUp = vehicle.transform.InverseTransformDirection(relativeUp);
            objectRelativeRight = vehicle.transform.InverseTransformDirection(relativeRight);
            objectRelativeForward = vehicle.transform.InverseTransformDirection(relativeForward);

            if(Input.GetKeyDown(KeyCode.W))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(objectRelativeUp.x), Mathf.Round(objectRelativeUp.y), Mathf.Round(objectRelativeUp.z)) * 90,Space.World);
            if (Input.GetKeyDown(KeyCode.S))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(-objectRelativeUp.x), Mathf.Round(-objectRelativeUp.y), Mathf.Round(-objectRelativeUp.z)) * 90, Space.World);
            if (Input.GetKeyDown(KeyCode.A))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(-objectRelativeRight.x), Mathf.Round(-objectRelativeRight.y), Mathf.Round(-objectRelativeRight.z)) * 90, Space.World);
            if (Input.GetKeyDown(KeyCode.D))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(objectRelativeRight.x), Mathf.Round(objectRelativeRight.y), Mathf.Round(objectRelativeRight.z)) * 90, Space.World);
            if(Input.GetKeyDown(KeyCode.Q))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(objectRelativeForward.x), Mathf.Round(objectRelativeForward.y), Mathf.Round(objectRelativeForward.z)) * 90, Space.World);
            if (Input.GetKeyDown(KeyCode.E))
                tBlock.transform.Rotate(new Vector3(Mathf.Round(-objectRelativeForward.x), Mathf.Round(-objectRelativeForward.y), Mathf.Round(-objectRelativeForward.z)) * 90, Space.World);

        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Q) && turretEditorBG.activeInHierarchy)
            {
                turretEditorBG.SetActive(false);
                mainCamera.GetComponent<CameraFollow>().locked = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (Input.GetKeyDown(KeyCode.Q) && roomEditorBG.activeInHierarchy)
            {
                roomEditorBG.SetActive(false);
                mainCamera.GetComponent<CameraFollow>().locked = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseAllMenus();
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            if(isMirrored)
            {
                isMirrored = false;
                mirrorObj.SetActive(false);
                placeTwo.SetActive(false);
            }
            else
            {
                placeTwo.SetActive(true);
                isMirrored = true;
                mirrorObj.SetActive(true);
            }
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            size = vehicle.GetDimensions();
            dimensionText.text = "Length: " + size.z + "\nWidth: " + size.x + "\nHeight: " + size.y;
        }
        if(Input.GetKeyDown(KeyCode.B)) //temporary to prevent overlap with RTS controller, both build and RTS controller will not be present at the same time
        {
            if (buildMode)
            {
                mainCamera.GetComponent<CameraFollow>().locked = false;
                buildMode = false;
            }
            else
            {
                mainCamera.GetComponent<CameraFollow>().locked = true;
                buildMode = true;
            }
        }

        ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (!buildBG.activeInHierarchy && Physics.Raycast(ray, out hit, Mathf.Infinity, placedMask))
        {
            if (hit.transform.GetComponent<Turret>() && Input.GetKeyDown(KeyCode.Q) && !Input.GetKey(KeyCode.Tab))
            {
                turretEditorBG.SetActive(true);
                editTurret = hit.transform.GetComponent<Turret>();
                mainCamera.GetComponent<CameraFollow>().locked = true;
                Cursor.lockState = CursorLockMode.None;
                GetTurretData();
            }

            if (hit.transform.GetComponent<Block>() && hit.transform.GetComponent<Block>().isRoom && Input.GetKeyDown(KeyCode.Q) && !Input.GetKey(KeyCode.Tab))
            {
                roomEditorBG.SetActive(true);
                mainCamera.GetComponent<CameraFollow>().locked = true;
                Cursor.lockState = CursorLockMode.None;
                editRoom = hit.transform.GetComponent<Block>().room;
                roomCountText.text = "Rooms: " + editRoom.roomBlocks.Count;
                GetRoomData();
            }

            if (buildMode)
            {
                tBlock.transform.position = hit.collider.transform.position + hit.normal;
                placeOne.GetComponent<Block>().mesh.GetComponent<Renderer>().material.color = new Color32(0, 255, 0, 255); //optimize by detecting movement?
                if (isMirrored)
                    placeTwo.GetComponent<Block>().mesh.GetComponent<Renderer>().material.color = new Color32(0, 255, 0, 255); //also cache to optimize 
                placeOne.transform.rotation = tBlock.transform.rotation;
                placeTwo.transform.rotation = new Quaternion(-tBlock.transform.rotation.x, tBlock.transform.rotation.y, tBlock.transform.rotation.z, -tBlock.transform.rotation.w);
                placeOne.transform.position = new Vector3(tBlock.transform.position.x, tBlock.transform.position.y, tBlock.transform.position.z) + (placeOne.transform.forward * (placeOne.transform.localScale.z - 1) * 0.5f);
                placeTwo.transform.position = new Vector3(-tBlock.transform.position.x, tBlock.transform.position.y, tBlock.transform.position.z) + (placeTwo.transform.forward * (placeTwo.transform.localScale.z - 1) * 0.5f);

                if (OverlapCheck(placeOne.GetComponent<Block>()))
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        place = Instantiate(blockDict.blockDict[currentBlockVal]) as GameObject;
                        place.SetActive(true);
                        place.transform.position = tBlock.transform.position + (tBlock.transform.forward * ((place.transform.localScale.z - 1) * 0.5f));
                        place.transform.rotation = tBlock.transform.rotation;
                        place.gameObject.layer = 8;
                        foreach (Transform child in place.transform)
                        {
                            child.gameObject.layer = 8;
                        }
                        place.GetComponent<Block>().CacheLocalPosition();
                        place.GetComponent<Block>().isBuildBlock = false;
                        place.transform.SetParent(vehicleObj.transform);
                        place.GetComponent<Block>().placeID = vehicle.blocks.Count;
                        vehicle.blocks.Add(place.GetComponent<Block>());
                        if (place.GetComponent<Block>().isRoom)
                            vehicle.RoomCombiner(place.GetComponent<Block>().placeID);

                        if (isMirrored)
                        {
                            if (OverlapCheck(placeTwo.GetComponent<Block>()))
                            {
                                place = Instantiate(blockDict.blockDict[currentBlockVal], tBlock.transform.position, tBlock.transform.rotation) as GameObject;
                                place.SetActive(true);
                                place.transform.localScale = new Vector3(place.transform.localScale.x * -1.0f, place.transform.localScale.y, place.transform.localScale.z);
                                place.transform.localRotation = new Quaternion(place.transform.localRotation.x * -1.0f, place.transform.localRotation.y, place.transform.localRotation.z, place.transform.localRotation.w * -1.0f);
                                place.transform.position = new Vector3(-1 * tBlock.transform.localPosition.x, tBlock.transform.position.y, tBlock.transform.position.z) + (place.transform.forward * ((place.transform.localScale.z - 1) * 0.5f));

                                place.gameObject.layer = 8;
                                foreach (Transform child in place.transform)
                                {
                                    child.gameObject.layer = 8;
                                    if (child.GetComponent<BoxCollider>())
                                    {
                                        child.localScale = new Vector3(-1.0f, 1.0f, 1 / place.transform.localScale.z);
                                    }
                                }
                                place.GetComponent<Block>().CacheLocalPosition();
                                place.GetComponent<Block>().isBuildBlock = false;
                                place.transform.SetParent(vehicleObj.transform);
                                place.GetComponent<Block>().placeID = vehicle.blocks.Count;
                                vehicle.blocks.Add(place.GetComponent<Block>());
                                if (place.GetComponent<Block>().isRoom)
                                    vehicle.RoomCombiner(place.GetComponent<Block>().placeID);
                            }
                        }
                        vehicle.DetectSurfaceBlocks();
                        vehicle.NewCenterOfMass();
                        vehicle.SetTotalVolume();
                        vehicle.SetMomentsOfInertia();
                        vehicle.UpdateOpenFaces();
                        vehicle.UpdateAngularDrag();
                        vehicle.GetAirDrag();

                        if (!vehicle.isConnected)
                        {
                            Timing.RunCoroutine(DisconnectCheck());
                        }
                    }
                }
                else
                {
                    placeOne.GetComponent<Block>().mesh.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255); //optimize by detecting movement?
                    if (isMirrored)
                        placeTwo.GetComponent<Block>().mesh.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255);
                }
                if (Input.GetMouseButtonDown(1))
                {
                    if (vehicle.blocks.Count > 1)
                    {
                        vehicle.blocks.RemoveAt(hit.transform.GetComponent<Block>().placeID);
                        vehicle.RenumberParts();
                        if (hit.transform.GetComponent<Block>().isRoom)
                        {
                            hit.transform.GetComponent<Block>().room.roomBlocks.Remove(hit.transform.GetComponent<Block>());
                            RemoveRoomCheck();
                        }
                        if (isMirrored && hit.transform.position.x != 0)
                        {
                            Collider[] hit2 = Physics.OverlapBox(new Vector3(-hit.transform.position.x, hit.transform.position.y, hit.transform.position.z), Vector3.one / 3, hit.transform.rotation, placedMask);
                            if (hit2.Length >= 1)
                            {
                                vehicle.blocks.RemoveAt(hit2[0].transform.parent.GetComponent<Block>().placeID);
                                if (hit2[0].transform.parent.GetComponent<Block>().isRoom)
                                {
                                    hit.transform.GetComponent<Block>().room.roomBlocks.Remove(hit.transform.GetComponent<Block>());
                                    RemoveRoomCheck();
                                }
                                Destroy(hit2[0].transform.parent.gameObject);
                            }
                        }
                        Destroy(hit.transform.gameObject);
                        if (vehicle.blocks.Count > 1)
                            Timing.RunCoroutine(DisconnectCheck());
                    }
                    else
                    {
                        ShowMessge("Cannot delete the last block!");
                    }
                }
            }
        }
    }

    void RemoveRoomCheck()
    {
        for(int i=0;i<vehicle.rooms.Count;i++)
        {
            if(vehicle.rooms[i].roomBlocks.Count == 0)
            {
                Rooms temp = vehicle.rooms[i];
                vehicle.rooms.Remove(temp);
                Destroy(temp);
            }
        }
    }

    void DeleteRooms()
    {
        for (int i = 0; i < vehicle.rooms.Count; i++)
        {
            Rooms temp = vehicle.rooms[i];
            vehicle.rooms.Remove(temp);
            Destroy(temp);
        }
    }

    IEnumerator<float> DisconnectCheck()
    {
        yield return Timing.WaitForOneFrame;
        vehicle.RenumberParts();
        vehicle.DisconnectTest();
        vehicle.DetectSurfaceBlocks();
        vehicle.NewCenterOfMass();
        vehicle.SetTotalVolume();
        vehicle.SetMomentsOfInertia();
        vehicle.UpdateOpenFaces();
        if (!vehicle.isConnected)
            ShowMessge("There are blocks disconnected from the ship!");
    }

    public void SaveShip() //Update to take into account functional blocks with editable values (tagging or sections)
    {
        if(vehicle.isConnected)
        {
            string file = shipNameInputField.text;
            if (file == "")
                file = "Untitled Ship";
            if (!Directory.Exists("Ships/"))
                Directory.CreateDirectory("Ships/");
            StreamWriter w = new StreamWriter("Ships/" + file + ".shp", false);
            vehicle.NewCenterOfMass();
            vehicle.FindTotalCost();
            w.WriteLine(shipNameInputField.text + "|" + 
                        vehicle.totalCost + "|" + 
                        vehicle.centerOfMass.x + "|" + 
                        vehicle.centerOfMass.y + "|" + 
                        vehicle.centerOfMass.z + "|" + 
                        vehicle.totalMass + "|" + 
                        vehicle.totalHP);
            for (int i = 0; i < vehicle.blocks.Count; i++) //blocks
            {
                if (!vehicle.blocks[i].isFunctional)
                {
                    Block temp = vehicle.blocks[i];
                    w.WriteLine(temp.id + "|" + 
                                temp.transform.localPosition.x + "|" + 
                                temp.transform.localPosition.y + "|" + 
                                temp.transform.localPosition.z + "|" + 
                                temp.transform.eulerAngles.x + "|" + 
                                temp.transform.eulerAngles.y + "|" + 
                                temp.transform.eulerAngles.z);
                }
            }
            w.WriteLine("#BLOCKSEND#");
            for (int i = 0; i < vehicle.blocks.Count; i++) //turrets
            {
                if (vehicle.blocks[i].isTurret)
                {
                    Block temp = vehicle.blocks[i];
                    Turret temp2 = vehicle.blocks[i].GetComponent<Turret>();
                    w.WriteLine(temp.id + "|" +
                                temp.transform.localPosition.x + "|" +
                                temp.transform.localPosition.y + "|" +
                                temp.transform.localPosition.z + "|" +
                                temp.transform.eulerAngles.x + "|" +
                                temp.transform.eulerAngles.y + "|" +
                                temp.transform.eulerAngles.z + "|" +
                                temp2.turretName + "|" +
                                temp2.barrelCount + "|" +
                                temp2.caliber + "|" +
                                temp2.length + "|" +
                                temp2.isAA);
                }
            }
            w.WriteLine("#TURRETSEND#");
            for(int i=0;i<vehicle.rooms.Count;i++) //rooms
            {
                w.WriteLine(vehicle.rooms[i].rid + "|" +
                            vehicle.rooms[i].roomType);
            }
            w.WriteLine("#ROOMSEND#");
            for (int i = 0; i < vehicle.blocks.Count; i++) //roomblocks
            {
                if (vehicle.blocks[i].isRoom)
                {
                    Block temp = vehicle.blocks[i];
                    w.WriteLine(temp.id + "|" +
                                temp.transform.localPosition.x + "|" +
                                temp.transform.localPosition.y + "|" +
                                temp.transform.localPosition.z + "|" +
                                temp.transform.eulerAngles.x + "|" +
                                temp.transform.eulerAngles.y + "|" +
                                temp.transform.eulerAngles.z + "|" +
                                temp.room.rid);
                }
            }
            w.WriteLine("#ROOMBLOCKSEND#");
            w.Close();
        }
        else
        {
            ShowMessge("Cannot Save: There are blocks disconnected from the ship!");
        }
    }

    public void ShowMessge(string output)
    {
        messageBG.SetActive(true);
        messageText.text = output;
        Timing.RunCoroutine(HideMessage());
    }

    IEnumerator<float> HideMessage()
    {
        yield return Timing.WaitForSeconds(5f);
        messageBG.SetActive(false);
    }

    public void LoadShip(string file)
    {
        string line;
        char[] delimiter = { '|' };

        int tempCounter = vehicle.blocks.Count;
        for (int i = 0; i < tempCounter; i++)
        {
            GameObject temp = vehicle.blocks[0].gameObject;
            vehicle.blocks.RemoveAt(0);
            Destroy(temp);
        }

        int tempPlaceID = 0;
        if (!Directory.Exists("Ships/"))
            Directory.CreateDirectory("Ships/");
        StreamReader s = File.OpenText("Ships/" + file);
        DeleteRooms();
        line = s.ReadLine();
        string[] tempString = line.Split(delimiter);
        vehicle.shipName = tempString[0];
        vehicle.totalCost = int.Parse(tempString[1]);
        vehicle.centerOfMass = new Vector3(float.Parse(tempString[2]), float.Parse(tempString[3]), float.Parse(tempString[4]));
        vehicle.totalMass = float.Parse(tempString[5]);
        vehicle.totalHP = int.Parse(tempString[6]);
        line = s.ReadLine();
        while (line != "#BLOCKSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            temp.layer = 8;
            foreach(Transform child in temp.transform)
            {
                if(child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#TURRETSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            temp.GetComponent<Turret>().turretName = tempString[7];
            temp.GetComponent<Turret>().barrelCount = int.Parse(tempString[8]);
            temp.GetComponent<Turret>().caliber = float.Parse(tempString[9]);
            temp.GetComponent<Turret>().length = float.Parse(tempString[10]);
            temp.GetComponent<Turret>().isAA = bool.Parse(tempString[11]);
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#ROOMSEND#")
        {
            tempString = line.Split(delimiter);
            Rooms temp = new Rooms();
            temp.rid = int.Parse(tempString[0]);
            temp.roomType = int.Parse(tempString[1]);
            vehicle.rooms.Add(temp);
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#ROOMBLOCKSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            int tempRID = int.Parse(tempString[7]);
            for(int x=0;x<vehicle.rooms.Count;x++)
            {
                if(tempRID == vehicle.rooms[x].rid)
                {
                    temp.GetComponent<Block>().room = vehicle.rooms[x];
                    vehicle.rooms[x].roomBlocks.Add(temp.GetComponent<Block>());
                }
            }
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }

        s.Close();

        shipNameInputField.text = file.Substring(0,file.Length-4);
        loadMenuBG.SetActive(false);

        Timing.RunCoroutine(FinalizeDelay());
    }

    IEnumerator<float> FinalizeDelay()
    {
        yield return Timing.WaitForOneFrame;
        vehicle.DetectSurfaceBlocks();
        vehicle.NewCenterOfMass();
        vehicle.SetTotalVolume();
        vehicle.SetMomentsOfInertia();
        vehicle.UpdateOpenFaces();
        vehicle.UpdateAngularDrag();
        vehicle.GetAirDrag();
    }

    public void LoadShipMenu()
    {
        loadMenuBG.SetActive(true);

        for (int i = 0; i < loadMenuButtonList.Count; i++)
        {
            Destroy(loadMenuButtonList[i].gameObject);
        }

        string[] files = Directory.GetFiles("Ships/");
        int offset = -60;
        int counter = 0;
        foreach (string file in files)
        {

            GameObject clone = Instantiate(loadShipButton) as GameObject;
            clone.transform.SetParent(loadMenuContent.transform);
            clone.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, offset,0);
            clone.name = Path.GetFileName(file);
            clone.transform.GetChild(0).GetComponent<Text>().text = clone.name;
            clone.GetComponent<LoadShipButton>().bc = GetComponent<BuildController>();
            loadMenuButtonList.Add(clone);
            offset -= 60;
            counter++;
        }
        loadMenuContent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 100 + (counter * 200));
    }

    public void SwitchBuildMenu(int i)
    {
        for (int x = 0; x < buildMenuList.Count; x++)
        {
            buildMenuList[x].SetActive(false);
        }
        buildMenuList[i].SetActive(true);
    }

    public void CloseAllMenus()
    {
        CloseLoadShipMenu();
        CloseBuildMenu();
    }

    public void CloseBuildMenu()
    {
        buildBG.SetActive(false);
    }

    public void CloseLoadShipMenu()
    {
        loadMenuBG.SetActive(false);
    }

    public void SelectBlock(int i)
    {
        Destroy(placeOne);
        Destroy(placeTwo);
        GameObject temp = Instantiate(blockDict.blockDict[i]);
        temp.SetActive(true);
        placeOne = temp;
        placeOne.GetComponent<Block>().isBuildBlock = true;
        placeOne.GetComponent<Block>().id = i;
        temp.transform.position = new Vector3(tBlock.transform.position.x, tBlock.transform.position.y, tBlock.transform.position.z+ (temp.transform.localScale.z-1) * 0.5f);
        temp.transform.rotation = tBlock.transform.rotation;
        temp.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color32(0, 255, 0, 255);
        temp.gameObject.layer = 9;
        temp = Instantiate(blockDict.blockDict[i]);
        temp.SetActive(true);
        placeTwo = temp;
        placeTwo.GetComponent<Block>().isBuildBlock = true;
        placeTwo.GetComponent<Block>().id = i;
        temp.transform.position = new Vector3(tBlock.transform.position.x, tBlock.transform.position.y, tBlock.transform.position.z + (temp.transform.localScale.z - 1) * 0.5f);
        temp.transform.rotation = tBlock.transform.rotation;
        temp.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color32(0, 255, 0, 255);
        temp.gameObject.layer = 9;
        placeTwo.transform.localScale = new Vector3(placeTwo.transform.localScale.x * -1.0f, placeTwo.transform.localScale.y, placeTwo.transform.localScale.z);
        if (!isMirrored)
            placeTwo.SetActive(false);
        currentBlockVal = i;
    }
}
