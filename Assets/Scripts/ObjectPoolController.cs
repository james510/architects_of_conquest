﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool : ScriptableObject
{
    public string poolName;
    public GameObject poolObject;
    private GameObject parent;
    private List<GameObject> poolList = new List<GameObject>();
    public List<GameObject> activeList = new List<GameObject>();
    public bool isMoving = false;
    public float speed;

    public void CreatePool(GameObject poolObject, int amount, string poolName, GameObject parent)
    {
        this.parent = parent;
        this.poolName = poolName;
        this.poolObject = poolObject;
        for (int i = 0; i < amount; i++)
        {
            GameObject clone = Instantiate(poolObject) as GameObject;
            clone.GetComponent<PoolObject>().pool = parent.GetComponent<ObjectPoolController>();
            clone.name = poolName;
            clone.transform.SetParent(parent.transform);
            clone.SetActive(false);
            poolList.Add(clone);
        }
    }

    public void ActivateAll()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            poolList[i].transform.position = new Vector2(10000, 10000);
            poolList[i].SetActive(true);
        }
    }

    public void ParentAll(GameObject obj)
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            poolList[i].transform.SetParent(obj.transform);
        }
    }

    public GameObject Spawn()
    {
        GameObject clone;
        if (poolList.Count < 1)
        {
            Debug.Log("Caution: Pool out of - " + poolObject.name + " - spawning a new one");
            clone = Instantiate(poolObject) as GameObject;
            clone.GetComponent<PoolObject>().pool = parent.GetComponent<ObjectPoolController>();
            clone.name = poolName;
            //clone.transform.SetParent(parent.transform);
            clone.SetActive(false);
        }
        else
        {
            clone = poolList[0];
            poolList.RemoveAt(0);
        }
        activeList.Add(clone);
        return clone;
    }

    public void Despawn(GameObject obj)
    {
        activeList.Remove(obj);
        poolList.Add(obj);
    }
};

public class ObjectPoolController : MonoBehaviour
{
    public List<Pool> pools = new List<Pool>();
    public GameObject bullet;
    public GameObject fxCannonShot, fxDeckHit, fxFire, fxWaterSplash, fxFlak, fxWake;
    private float deltaTimeRef;
    private float speed;
    private List<GameObject> tempActive;

    void Awake()
    {
        
        Pool bulletPool = ScriptableObject.CreateInstance<Pool>();
        bulletPool.CreatePool(bullet, 200, "Bullet", gameObject);
        pools.Add(bulletPool);

        Pool fxCannonShotPool = ScriptableObject.CreateInstance<Pool>();
        fxCannonShotPool.CreatePool(fxCannonShot, 200, "fxCannonShot", gameObject);
        pools.Add(fxCannonShotPool);

        Pool fxDeckHitPool = ScriptableObject.CreateInstance<Pool>();
        fxDeckHitPool.CreatePool(fxDeckHit, 200, "fxDeckHit", gameObject);
        pools.Add(fxDeckHitPool);

        Pool fxFirePool = ScriptableObject.CreateInstance<Pool>();
        fxFirePool.CreatePool(fxFire, 200, "fxFire", gameObject);
        pools.Add(fxFirePool);

        Pool fxWaterSplashPool = ScriptableObject.CreateInstance<Pool>();
        fxWaterSplashPool.CreatePool(fxWaterSplash, 200, "fxWaterSplash", gameObject);
        pools.Add(fxWaterSplashPool);

        Pool fxFlakPool = ScriptableObject.CreateInstance<Pool>();
        fxFlakPool.CreatePool(fxFlak, 200, "fxFlak", gameObject);
        pools.Add(fxFlakPool);

        Pool fxWakePool = ScriptableObject.CreateInstance<Pool>();
        fxWakePool.CreatePool(fxWake, 200, "fxWake", gameObject);
        pools.Add(fxWakePool);


    }

    void Update()
    {
        deltaTimeRef = Time.deltaTime;
        for(int x=0;x<pools.Count;x++)
        {
            if (pools[x].isMoving)
            {
                speed = pools[x].speed;
                tempActive = pools[x].activeList;
                for (int i = 0; i < pools[x].activeList.Count; i++)
                {
                    tempActive[i].transform.Translate((Vector3.forward * speed) * deltaTimeRef);
                }
            }
        }
    }

    public GameObject Spawn(string poolName)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].poolName == poolName)
            {
                return pools[i].Spawn();
            }
        }
        Debug.Log("Could not find pool or object");
        return null;
    }

    public void ActivateAll(string poolName)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].poolName == poolName)
            {
                pools[i].ActivateAll();
                break;
            }
        }
        //Debug.Log("Could not find pool or object");
    }

    public void ParentAll(string poolName, GameObject obj)
    {
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].poolName == poolName)
            {
                pools[i].ParentAll(obj);
                break;
            }
        }
    }

    public void Despawn(string objName, GameObject obj)
    {
        bool found = false;
        for (int i = 0; i < pools.Count; i++)
        {
            if (pools[i].poolName == objName)
            {
                pools[i].Despawn(obj);
                found = true;
                break;
            }
        }
        if (!found)
        {
            Destroy(obj);
            Debug.Log("Warning: pool not found for : " + objName);
        }
    }
}
