﻿using UnityEngine;
using System.Collections;

public class Rudder : Block
{
    public float baseForce=10, waterVelocity, force;
    int propellerDistance;
    Ray ray;
    RaycastHit hit;

    public void Start()
    {
        transform.localRotation.Equals(Quaternion.Euler(0, 0, 0));
        ray = new Ray(transform.position, transform.forward);
        CheckPropellers();
    }

    public void Update()
    {
        if (!isBuildBlock)
        {
            waterVelocity = transform.parent.InverseTransformVector(transform.parent.GetComponent<Vehicle>().velocity).z;
            if (propellerDistance > 0)
            {
                waterVelocity += transform.parent.GetComponent<Vehicle>().throttle * hit.transform.GetComponent<Thruster>().thrust / (5 * propellerDistance);
            }

            force = baseForce * waterVelocity;
        }
    }

    public void CheckPropellers()
    {
        if (Physics.Raycast(ray, out hit, 10, placedMask))
        {
            if (hit.transform.GetComponent<Thruster>())
                propellerDistance = Mathf.Abs(Mathf.RoundToInt(hit.transform.localPosition.z - transform.localPosition.z));
        }
        else
            propellerDistance = 0;
    }
}
