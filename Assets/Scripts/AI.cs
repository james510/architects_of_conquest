﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    CharacterController charCtrl;
    int placedMask = 1 << 8;
    Vector3 alliedcenterofmass = Vector3.zero, enemycenterofmass = Vector3.zero;
    int stance = 1; // neutral = 0, aggressive = 1, def = 2
    //stances, actions
    //aggressive stance/ hastorpedoes= true, shoot torpedoes and stay within firing range
    //neutral stance  stay where you are and attack  
    //defensive to stay ont he outside of fire
    // center of mass of all ships to determine how far from the center of mass we can go

    //actions override stances()
    void CentersofMass()
    {
        float totalmassa=0, totalmasse=0;
        float xa = 0, za = 0, xe = 0, ze=0;
        Vector3 p1 = transform.position + charCtrl.center;
        RaycastHit[] vehicle = Physics.SphereCastAll(p1, 15000, transform.forward, Mathf.Infinity, placedMask, QueryTriggerInteraction.UseGlobal);

        for( int i = 0; i < vehicle.Length; i++)
        {
            if (vehicle[i].transform.GetComponent<Vehicle>().team == transform.GetComponent<Vehicle>().team)
            {
                xa += vehicle[i].transform.position.x*vehicle[i].transform.GetComponent<Vehicle>().totalMass;
                za += vehicle[i].transform.position.z*vehicle[i].transform.GetComponent<Vehicle>().totalMass;
                totalmassa += vehicle[i].transform.GetComponent<Vehicle>().totalMass;
            }
            else if(!(vehicle[i].transform.GetComponent<Vehicle>().team == transform.GetComponent<Vehicle>().team) && transform.GetComponent<Vehicle>().team != 0)
            {
                xe += vehicle[i].transform.position.x * vehicle[i].transform.GetComponent<Vehicle>().totalMass;
                ze += vehicle[i].transform.position.z * vehicle[i].transform.GetComponent<Vehicle>().totalMass;
                totalmasse += vehicle[i].transform.GetComponent<Vehicle>().totalMass;
            }
        }
        xa /= totalmassa;
        za /= totalmassa;
        alliedcenterofmass.x = xa;
        alliedcenterofmass.z = za;
        xe /= totalmasse;
        ze /= totalmasse;
        enemycenterofmass.x = xe;
        enemycenterofmass.z = ze;
    }
    void Aggressive()
    {

        Vector3 targetposition = enemycenterofmass - transform.position;
        transform.GetComponent<Vehicle>().rudder = Mathf.Sign(targetposition.x);
    }
    void Neutral()
    {

    }
    void Defensive()
    {
        Vector3 targetposition = alliedcenterofmass - transform.position;
        transform.GetComponent<Vehicle>().rudder = Mathf.Sign(targetposition.x);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CentersofMass();
        if (stance == 1)
        {
            Neutral();
        }
        else if (stance == 2)
        {
            Aggressive();
        }
        else
        {
            Defensive();
        }
	}
}
