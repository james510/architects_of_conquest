﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceVector : ScriptableObject
{
    public Vector3 position, force, moment;

    public void Init(Vector3 pos, Vector3 f)
    {
        position = pos;
        force = f;
    }

    public void UpdateVectors(Vector3 f)
    {
        force = f;
        moment = Vector3.Cross(position, force);
    }

    public Vector3 GetMoment()
    {
        return moment = Vector3.Cross(position, force);
    }
}
