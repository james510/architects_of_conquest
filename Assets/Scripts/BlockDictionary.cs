﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class BlockDictionary : MonoBehaviour
{
    public Dictionary<int, GameObject> blockDict = new Dictionary<int, GameObject>();
    public Material debugTexure,woodTexture,metalTexture;
    public List<string> materialList = new List<string>();
    public List<Material> materialTextures = new List<Material>();

    public GameObject debugBlock,debugTriangle,debugInvTri,debugSquareTri,debugWedge,debugTriCorner;
    public GameObject debugBlock2, debugTriangle2, debugInvTri2, debugSquareTri2, debugWedge2, debugTriCorner2;
    public GameObject debugBlock3, debugTriangle3, debugInvTri3, debugSquareTri3, debugWedge3, debugTriCorner3;
    public GameObject debugBlock4, debugTriangle4, debugInvTri4, debugSquareTri4, debugWedge4, debugTriCorner4;
    public GameObject debugTurret,debugRoom,debugPropeller,debugRudder,debugTurretSub3,debugTurretSub5;

    void Awake ()
    {
        materialTextures.Add(debugTexure);
        materialTextures.Add(woodTexture);
        materialTextures.Add(metalTexture);
        GetMaterials();

        blockDict.Add(blockDict.Count, debugTurret);
        blockDict.Add(blockDict.Count, debugRoom);
        blockDict.Add(blockDict.Count, debugPropeller);
        blockDict.Add(blockDict.Count, debugRudder);
        blockDict.Add(blockDict.Count, debugTurretSub3);

        for (int i = 0; i < blockDict.Count; i++)
        {
            blockDict[i].GetComponent<MeshCollider>().enabled = false;
            blockDict[i].gameObject.SetActive(false);
        }
    }

    void AddMaterial(string name, float density, int health, int armor, int cost, Material material)
    {
        //Debug.Log(name + density + health + armor + cost + material);

        int id = blockDict.Count;
        Dictionary<int, GameObject> temp = new Dictionary<int, GameObject>();

        GameObject block = Instantiate(debugBlock);
        GameObject block2 = Instantiate(debugBlock2);
        GameObject block3 = Instantiate(debugBlock3);
        GameObject block4 = Instantiate(debugBlock4);
        GameObject tri = Instantiate(debugTriangle);
        GameObject tri2 = Instantiate(debugTriangle2);
        GameObject tri3 = Instantiate(debugTriangle3);
        GameObject tri4 = Instantiate(debugTriangle4);
        GameObject triCorner = Instantiate(debugTriCorner);
        GameObject triCorner2 = Instantiate(debugTriCorner2);
        GameObject triCorner3 = Instantiate(debugTriCorner3);
        GameObject triCorner4 = Instantiate(debugTriCorner4);
        GameObject invTri = Instantiate(debugInvTri);
        GameObject invTri2 = Instantiate(debugInvTri2);
        GameObject invTri3 = Instantiate(debugInvTri3);
        GameObject invTri4 = Instantiate(debugInvTri4);
        GameObject squareTri = Instantiate(debugSquareTri);
        GameObject squareTri2 = Instantiate(debugSquareTri2);
        GameObject squareTri3 = Instantiate(debugSquareTri3);
        GameObject squareTri4 = Instantiate(debugSquareTri4);
        GameObject wedge = Instantiate(debugWedge);
        GameObject wedge2 = Instantiate(debugWedge2);
        GameObject wedge3 = Instantiate(debugWedge3);
        GameObject wedge4 = Instantiate(debugWedge4);

        temp.Add(0, block);
        temp.Add(1, block2);
        temp.Add(2, block3);
        temp.Add(3, block4);
        temp.Add(4, tri);
        temp.Add(5, tri2);
        temp.Add(6, tri3);
        temp.Add(7, tri4);
        temp.Add(8, triCorner);
        temp.Add(9, triCorner2);
        temp.Add(10, triCorner3);
        temp.Add(11, triCorner4);
        temp.Add(12, invTri);
        temp.Add(13, invTri2);
        temp.Add(14, invTri3);
        temp.Add(15, invTri4);
        temp.Add(16, squareTri);
        temp.Add(17, squareTri2);
        temp.Add(18, squareTri3);
        temp.Add(19, squareTri4);
        temp.Add(20, wedge);
        temp.Add(21, wedge2);
        temp.Add(22, wedge3);
        temp.Add(23, wedge4);

        for (int i = 0; i < 24; i++)
        {
            temp[i].GetComponent<Block>().mass = density * temp[i].GetComponent<Block>().volume;
            temp[i].GetComponent<Block>().hp = health * Convert.ToInt32(temp[i].GetComponent<Block>().volume);
            temp[i].GetComponent<Block>().cost = cost * Convert.ToInt32(temp[i].GetComponent<Block>().volume);
            temp[i].GetComponent<Block>().armor = armor;
            temp[i].GetComponent<Block>().id = id + i;

            temp[i].name = name + " " + temp[i].name;
            temp[i].GetComponent<Block>().blockName = name + " " + temp[i].GetComponent<Block>().blockName;
            temp[i].GetComponent<Block>().mesh.GetComponent<Renderer>().material = material;
            temp[i].GetComponent<Block>().isBuildBlock = true;
            blockDict.Add(id + i, temp[i]);
        }
    }

    void GetMaterials()
    {
        string line;
        string[] tempString;
        char[] delimiter = { '|' };
        float density;
        int health, armor, cost;
        string name;
        Material material;

        try
        {
            using (StreamReader s = File.OpenText("Data/" + "materiallist.txt"))
            {
                line = s.ReadLine();
                while (line != null)
                {
                    if (line.Substring(0, 2) == "//")
                        line = s.ReadLine();
                    tempString = line.Split(delimiter);
                    if (tempString.Length == 6)
                    {
                        name = tempString[0];
                        density = float.Parse(tempString[1]);
                        health = int.Parse(tempString[2]);
                        armor = int.Parse(tempString[3]);
                        cost = int.Parse(tempString[4]);
                        bool newSet = bool.Parse(tempString[5]);
                        materialList.Add(name);
                        //material = Resources.Load<Material>("Materials/" + name) as Material;
                        if(newSet)
                        {
                            material = null;
                            for (int x = 0; x < materialTextures.Count; x++)
                            {
                                if (name == materialTextures[x].name)
                                {
                                    material = materialTextures[x];
                                    break;
                                }
                            }

                            if (material == null)
                            {
                                AddMaterial(name, density, health, armor, cost, debugTexure);
                                Console.WriteLine("you fucked up");
                            }
                            else if (material != null)
                                AddMaterial(name, density, health, armor, cost, material);
                        }
                    }
                    else if (tempString.Length != 6)
                    {
                        Console.WriteLine("bad column count");
                    }

                    line = s.ReadLine();
                }
            }
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine(ex);
        }
    }
}
