﻿using UnityEngine;
using UnityEditor;

public class Thruster : Block
{
    public float thrust;

    public void SetOutput(float output)
    {
        thrust = output;
    }
}