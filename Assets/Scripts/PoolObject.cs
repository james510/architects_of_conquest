﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class PoolObject : MonoBehaviour
{
    public ObjectPoolController pool;
    public float despawnTime = 0f;

    void OnEnable()
    {
        if (despawnTime != 0)
            Timing.RunCoroutine(DespawnTimer());
    }

    IEnumerator<float> DespawnTimer()
    {
        yield return Timing.WaitForSeconds(despawnTime);
        Despawn();
    }

    public void Despawn()
    {
        //Timing.KillCoroutines();
        if (pool != null)
            pool.Despawn(gameObject.name, gameObject);
        else
            Debug.Log(gameObject.name + " Does not have a pool controller!");
        gameObject.SetActive(false);
    }
}
