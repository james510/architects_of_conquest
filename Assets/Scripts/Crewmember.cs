﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crew
{
    List<Crewmember> armCrewList = new List<Crewmember>();
    List<Crewmember> repCrewList = new List<Crewmember>();
    List<Crewmember> pilotCrewList = new List<Crewmember>();

    int armCrew = 0, repCrew = 0, pilotCrew = 0;
    int crewLevel = 1;
    int crewExp = 0;
    int morale = 75;

    void CountCrew()
    {
        armCrew = 0;
        repCrew = 0;
        pilotCrew = 0;

        for (int i = 0; i < armCrewList.Count; i++)
        {
            if (armCrewList[i].isAlive)
                    armCrew++;
        }
        for (int i = 0; i < repCrewList.Count; i++)
        {
            if (repCrewList[i].isAlive)
                repCrew++;
        }
        for (int i = 0; i < pilotCrewList.Count; i++)
        {
            if (pilotCrewList[i].isAlive)
                pilotCrew++;
        }
    }
};

public class Crewmember
{
    public bool isAlive = true;
    public int age, timeServed;
    public int type = 0; //0: armaments 1: damage control 2: pilot
    public string name = "John Doe";
};
