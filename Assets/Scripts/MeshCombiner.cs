﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCombiner : MonoBehaviour
{
    List<CombineInstance> debugList = new List<CombineInstance>();
    List<CombineInstance> woodList = new List<CombineInstance>();
    List<CombineInstance> metalList = new List<CombineInstance>();
    List<CombineInstance> roomList = new List<CombineInstance>();

    MeshFilter meshFilter;
    CombineInstance combine;
    MeshRenderer meshRender;
    string materialName;
    //http://www.habrador.com/tutorials/unity-mesh-combining-tutorial/3-combine-meshes-colors/

    public void CombineMeshes(GameObject obj)
    {
        Matrix4x4 myTransform = obj.transform.worldToLocalMatrix;
        MeshFilter[] meshFilters = obj.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        int i = 0;
        while (i < meshFilters.Length)//rewrite to account for rotation
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix * myTransform;
            meshFilters[i].gameObject.SetActive(false);
            i++;
        }
        obj.transform.GetComponent<MeshFilter>().mesh = new Mesh();
        obj.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        obj.transform.gameObject.SetActive(true);

        Debug.Log("Combine meshes ran, are you sure you want this to happen?");
    }

    public void CombineMeshesMultiMat(Vehicle vehicle)
    {
        debugList.Clear();
        woodList.Clear();
        metalList.Clear();
        roomList.Clear();
        Matrix4x4 myTransform = vehicle.transform.worldToLocalMatrix;
        for (int i = 0; i < vehicle.blocks.Count; i++)
        {
            if(!vehicle.blocks[i].isFunctional) //change to allow rooms as well later on
            {
                meshFilter = vehicle.blocks[i].mesh.GetComponent<MeshFilter>();
                combine = new CombineInstance();
                meshRender = meshFilter.GetComponent<MeshRenderer>();
                materialName = meshRender.material.name.Replace(" (Instance)", "");

                if (materialName == "Debug")
                {
                    combine.mesh = meshFilter.mesh;
                    combine.transform = Matrix4x4.TRS(vehicle.blocks[i].transform.localPosition, vehicle.blocks[i].transform.localRotation, vehicle.blocks[i].transform.localScale);
                    debugList.Add(combine);
                    vehicle.blocks[i].mesh.SetActive(false);
                }
                else if (materialName == "Wood")
                {
                    combine.mesh = meshFilter.mesh;
                    combine.transform = Matrix4x4.TRS(vehicle.blocks[i].transform.localPosition, vehicle.blocks[i].transform.localRotation, vehicle.blocks[i].transform.localScale);
                    woodList.Add(combine);
                    vehicle.blocks[i].mesh.SetActive(false);
                }
                else if (materialName == "Metal")
                {
                    combine.mesh = meshFilter.mesh;
                    combine.transform = Matrix4x4.TRS(vehicle.blocks[i].transform.localPosition, vehicle.blocks[i].transform.localRotation, vehicle.blocks[i].transform.localScale);
                    metalList.Add(combine);
                    vehicle.blocks[i].mesh.SetActive(false);
                }
                else if (materialName == "Room")
                {
                    combine.mesh = meshFilter.mesh;
                    combine.transform = Matrix4x4.TRS(vehicle.blocks[i].transform.localPosition, vehicle.blocks[i].transform.localRotation, vehicle.blocks[i].transform.localScale);
                    roomList.Add(combine);
                    vehicle.blocks[i].mesh.SetActive(false);
                }
            }
        }
        Mesh combinedDebugMesh = new Mesh();
        combinedDebugMesh.CombineMeshes(debugList.ToArray());

        Mesh combinedWoodMesh = new Mesh();
        combinedWoodMesh.CombineMeshes(woodList.ToArray());

        Mesh combinedMetalMesh = new Mesh();
        combinedMetalMesh.CombineMeshes(metalList.ToArray());

        Mesh combinedRoomMesh = new Mesh();
        combinedRoomMesh.CombineMeshes(roomList.ToArray());

        CombineInstance[] totalMesh = new CombineInstance[4];

        totalMesh[0].mesh = combinedDebugMesh;
        totalMesh[0].transform = vehicle.transform.localToWorldMatrix * myTransform;
        totalMesh[1].mesh = combinedWoodMesh;
        totalMesh[1].transform = vehicle.transform.localToWorldMatrix * myTransform;
        totalMesh[2].mesh = combinedMetalMesh;
        totalMesh[2].transform = vehicle.transform.localToWorldMatrix * myTransform;
        totalMesh[3].mesh = combinedRoomMesh;
        totalMesh[3].transform = vehicle.transform.localToWorldMatrix * myTransform;

        Mesh combinedAllMesh = new Mesh();

        combinedAllMesh.CombineMeshes(totalMesh, false);
        vehicle.GetComponent<MeshFilter>().mesh = combinedAllMesh;
        vehicle.GetComponent<MeshFilter>().mesh.RecalculateBounds();
        vehicle.GetComponent<MeshFilter>().mesh.RecalculateNormals();
        Destroy(combinedDebugMesh);
        Destroy(combinedWoodMesh);
        Destroy(combinedMetalMesh);
        Destroy(combinedRoomMesh);
    }
}
