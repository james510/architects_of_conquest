﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rooms : ScriptableObject
{
    public List<Block> roomBlocks = new List<Block>();
    public int rid = 0;
    public int roomType = 0;
    public float displacement = 0, height = 0, volume = 0;
    public bool isOnFire = false, isFlooding = false;
    public Vector3 centroid = Vector3.zero;
    float max, min;
    float worldHeight;

    public void Awake()
    {
        UpdateCentroid();
    }

    public void Update()
    {
        UpdateDisplacement();
    }

    public void UpdateCentroid()
    {
        Vector3 totalScaled = Vector3.zero;
        max = Mathf.NegativeInfinity;
        min = Mathf.Infinity;
        volume = 0;

        for(int i = 0; i < roomBlocks.Count; i++)
        {
            if (roomBlocks[i].transform.localPosition.y < min)
            {
                min = roomBlocks[i].transform.localPosition.y;
            }
            if (roomBlocks[i].transform.localPosition.y > max)
            {
                max = roomBlocks[i].transform.localPosition.y;
            }
            totalScaled += roomBlocks[i].transform.localPosition;
            volume++;
        }

        centroid = totalScaled / roomBlocks.Count;
        height = max - min;
    }

    public void UpdateDisplacement()
    {
        worldHeight = roomBlocks[0].transform.parent.transform.position.y + centroid.y;

        if (worldHeight < (height / 2) && worldHeight > -(height / 2))
        {
            displacement = volume * ((height / 2 - worldHeight) / height);
        }
        else if (worldHeight < -(height / 2))
        {
            displacement = volume;
        }
        else
            displacement = 0;
    }
}