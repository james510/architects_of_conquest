﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildMatButton : MonoBehaviour
{
    BuildController bc;
    public int i=0;
	// Use this for initialization
	void Start ()
    {
        bc = FindObjectOfType<BuildController>();
        GetComponent<Button>().onClick.AddListener(LoadBlock);
    }

    void LoadBlock()
    {
        bc.SwitchBuildMenu(i);
    }

}
