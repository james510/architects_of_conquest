﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class Block : MonoBehaviour
{
    public int id,hp,armor,placeID,cost;
    public int placedMask = 1 << 8;
    public float mass, volume, displacement;

    //index 0 is front, 1 is rear, 2 is right, 3 is left, 4 is up, 5 is down
    public Vector3 CDrag = Vector3.zero;
    public float[] baseDrag = new float[6]; //drag coefficient times area of side
    public bool[] openFaces = new bool[6];
    public bool isSurface = true;
    public bool isChecked = false;
    public bool isRoom = false;
    public bool isThruster = false;
    public bool isRudder = false;
    public bool isFunctional = false;
    public bool isTurret = false;
    public Rooms room;
    float air = GlobalVars.airDensity;
    public Color defaultColor;
    public Vector3 localPosCache;
    public List<GameObject> blockChild = new List<GameObject>();
    public GameObject mesh;
    public List<Block> adjacent = new List<Block>();
    public string blockName = "Default";
    public bool isBuildBlock = false;
    public bool isTurretSubstructure = false;
    public int subSize = 1;

    void Awake()
    {
        if (isRoom | isThruster | isRudder | isTurret)
            isFunctional = true;
        openFaces = new bool[6];
        defaultColor = transform.GetChild(0).GetComponent<Renderer>().material.color;
        if (!isRoom)
            displacement = volume;
        Timing.RunCoroutine(DelayCheck());
        blockChild.Clear();
        foreach (Transform child in transform)
        {
            if (child.GetComponent<BoxCollider>())
            {
                blockChild.Add(child.gameObject);
            }
            else
            {
                if(child.GetComponent<MeshRenderer>())
                    mesh = child.gameObject;
            }
        }
    }

    private void Start()
    {
        for(int i = 0; i < 6; i++)
        {
            baseDrag[i] = 0;
        }
    }

    IEnumerator<float> DelayCheck()
    {
        yield return Timing.WaitForOneFrame;
        if (!isBuildBlock)
            UpdateOpenFaces();
    }
    public void CacheLocalPosition()
    {
        localPosCache = transform.localPosition;
    }

    /*public void UpdateDrag(Vector3 parentVelocity)
    {
        dragForce = Vector3.zero;
        if (openFaces[0])
            if (parentVelocity.z >= 0)
                dragForce.z = -baseDrag[0] * air * parentVelocity.z * parentVelocity.z / 2;
        if (openFaces[1])
            if (parentVelocity.z < 0)
                dragForce.z = baseDrag[1] * air * parentVelocity.z * parentVelocity.z / 2;
        if (openFaces[2])
            if (parentVelocity.x >= 0)
                dragForce.x = -baseDrag[2] * air * parentVelocity.x * parentVelocity.x / 2;
        if (openFaces[3])
            if (parentVelocity.x < 0)
                dragForce.x = baseDrag[3] * air * parentVelocity.x * parentVelocity.x / 2;
        if (openFaces[4])
            if (parentVelocity.y >= 0)
                dragForce.y = -baseDrag[4] * air * parentVelocity.y * parentVelocity.y / 2;
        if (openFaces[5])
            if (parentVelocity.y < 0)
                dragForce.y = baseDrag[5] * air * parentVelocity.y * parentVelocity.y / 2;

        drag.Init(transform.localPosition, dragForce);
        velocity = parentVelocity;

        //Debug.Log(dragForce.y = -baseDrag[4] * air * parentVelocity.y * parentVelocity.y / 2);
    }*/

    /*public void UpdateDrag(Vector3 parentVelocity)
    {
        CDrag = Vector3.zero;
        if (openFaces[0])
            if (parentVelocity.z > 0)
                CDrag.z = baseDrag[0] * air / 2;
        if (openFaces[1])
            if (parentVelocity.z < 0)
                CDrag.z = baseDrag[1] * air / 2;
        if (openFaces[2])
            if (parentVelocity.x > 0)
                CDrag.x = baseDrag[2] * air / 2;
        if (openFaces[3])
            if (parentVelocity.x < 0)
                CDrag.x = baseDrag[3] * air / 2;
        if (openFaces[4])
            if (parentVelocity.y > 0)
                CDrag.y = baseDrag[4] * air / 2;
        if (openFaces[5])
            if (parentVelocity.y < 0)
                CDrag.y = baseDrag[5] * air / 2;
    }*/

    public void UpdateOpenFaces()
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<BoxCollider>())
            {
                if (Physics.OverlapBox(child.transform.position + transform.parent.transform.forward, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[0] = false;
                else
                    openFaces[0] = true;
                if (Physics.OverlapBox(child.transform.position - transform.parent.transform.forward, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[1] = false;
                else
                    openFaces[1] = true;
                if (Physics.OverlapBox(child.transform.position + transform.parent.transform.right, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[2] = false;
                else
                    openFaces[2] = true;
                if (Physics.OverlapBox(child.transform.position - transform.parent.transform.right, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[3] = false;
                else
                    openFaces[3] = true;
                if (Physics.OverlapBox(child.transform.position + transform.parent.transform.up, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[4] = false;
                else
                    openFaces[4] = true;
                if (Physics.OverlapBox(child.transform.position - transform.parent.transform.up, Vector3.one / 3, transform.rotation, placedMask).Length > 0)
                    openFaces[5] = false;
                else
                    openFaces[5] = true;
            }
        }
    }

    public void Damage(int damage)
    {
        hp -= damage;
    }
}
