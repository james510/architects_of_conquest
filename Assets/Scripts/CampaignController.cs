﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class StoredBlock
{
    public int id, hp;
    public float x, y, z;
    public float rotX, rotY, rotZ;

    public StoredBlock() { }
};

public class StoredVehicle
{
    public List<StoredBlock> blocks = new List<StoredBlock>();
    public Crew crew;
    public string shipName;
    public int totalHP, currentHP;


    public StoredVehicle()
    {

    }
};

public class StoredFleet
{
    public List<StoredVehicle> ships = new List<StoredVehicle>();
    public string fleetName;
}

public class CampaignController : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
