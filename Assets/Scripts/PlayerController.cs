﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vehicle vehicle;
	// Use this for initialization
	void Start ()
    {
        vehicle = GetComponent<Vehicle>();	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Input.GetKey(KeyCode.UpArrow))
        {
            vehicle.throttle = Mathf.Clamp(vehicle.throttle, 0, 1) + 0.1f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            vehicle.throttle = Mathf.Clamp(vehicle.throttle, 0, 1) - 0.1f * Time.deltaTime;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            vehicle.rudder = Mathf.Clamp(vehicle.rudder, -1, 1) - 0.1f * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            vehicle.rudder = Mathf.Clamp(vehicle.rudder, -1, 1) + 0.1f * Time.deltaTime;
        }
    }
}
