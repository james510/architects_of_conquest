﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;


public class Bullet : MonoBehaviour
{
    public Vector3 Velocity = new Vector3();
    public float speed, mass, kedmg, hedmg, destroyTime;
    public bool isSplash = false;
    PoolObject poolObject;
    Vector3 deltaPos;
    int placedMask = 1 << 8;

    void Awake()
    {
        poolObject = GetComponent<PoolObject>();
    }

    void OnEnable()
    {
        isSplash = false;
    }

    public void StartRound()
    {
        Timing.RunCoroutine(DestroyTimer());
    }

    IEnumerator<float> DestroyTimer()
    {
        yield return Timing.WaitForSeconds(destroyTime);
        Destroy(gameObject);
    }

    RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        float deltaTimeRef = Time.deltaTime;
        Velocity.y -= 9.81f * deltaTimeRef;
        speed = Velocity.magnitude;
        transform.rotation = Quaternion.LookRotation(Velocity);
        transform.Translate(deltaPos, Space.World);
        deltaPos = Velocity * deltaTimeRef;
        kedmg = mass * speed * speed / 2;

        if(transform.position.y<0 && isSplash==false)
        {
            isSplash = true;
            GameObject clone = GetComponent<PoolObject>().pool.Spawn("fxWaterSplash");
            clone.transform.position = transform.position;
            clone.SetActive(true);
            Timing.RunCoroutine(DespawnDelay());
        }
        if(Physics.Raycast(transform.position,transform.forward,out hit,1,placedMask))
        {
            if(hit.transform.GetComponent<Block>())
            {
                hit.transform.GetComponent<Block>().Damage(10);
                GameObject clone = GetComponent<PoolObject>().pool.Spawn("fxDeckHit");
                clone.transform.position = transform.position;
                clone.SetActive(true);
                poolObject.Despawn();
            }
        }
    }

    IEnumerator<float> DespawnDelay()
    {
        yield return Timing.WaitForSeconds(1f);
        poolObject.Despawn();
    }
}