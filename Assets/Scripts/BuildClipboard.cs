﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildClipboard : MonoBehaviour
{
    public string turretName = "Default";
    public int barrelCount = 1;
    public float caliber = 1, length = 1;
    public bool isAA = false;
}
