﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

public class CombatUnitManager : MonoBehaviour
{
    public Vehicle[] vehicles;
    public List<Vehicle> alliedVehicles = new List<Vehicle>();
    public List<Vehicle> enemyVehicles = new List<Vehicle>();
	
    void Start()
    {
        Timing.RunCoroutine(VehicleScanTimer());
    }

    IEnumerator<float> VehicleScanTimer()
    {
        yield return Timing.WaitForSeconds(1);
        VehicleScan();
        Timing.RunCoroutine(VehicleScanTimer());
    }

    public Vehicle[] GetVehicles()
    {
        return vehicles;
    }

    public List<Vehicle> GetAlliedVehicles()
    {
        return alliedVehicles;
    }

    public List<Vehicle> GetEnemyVehicles()
    {
        return enemyVehicles;
    }

    void VehicleScan()
    {
        vehicles = FindObjectsOfType<Vehicle>();
        alliedVehicles.Clear();
        enemyVehicles.Clear();
        for(int i=0;i<vehicles.Length;i++)
        {
            if (vehicles[i].isTeam)
                alliedVehicles.Add(vehicles[i]);
            else
                enemyVehicles.Add(vehicles[i]);
        }
    }
}
