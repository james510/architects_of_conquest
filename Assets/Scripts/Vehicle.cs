﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class Vehicle : MonoBehaviour
{
    public string shipName;
    public List<Block> blocks = new List<Block>();
    public ForceVector weight, buoyancy, engineForces, rudderForces;       //basic forces that are a property of the vehicle
    public float wt; //weight
    public float totalMass, totalVolume, totalDisplacement, maxThrust, throttle = 1, maxRudder, rudder = 0;
    public Vector3 reportDrag, reportBuoyancy;
    public Vector3 centerOfMass = new Vector3();        //the point where weight vector acts
    public Vector3 centerOfBuoy = new Vector3();        //the point where buoyancy vector acts
    public Vector3 centerOfDrag = new Vector3();        //the point where drag vector acts
    public Vector3 centerOfThrust = new Vector3();
    public Vector3 dragCoefficient = new Vector3();
    public Vector3 dragForce = new Vector3();
    public Vector3 dragAcceleration = new Vector3();
    public Vector3 angularDrag = new Vector3();
    public Vector3 dragXPlus, dragXMinus, dragYPlus, dragYMinus, dragZPlus, dragZMinus;
    public float[] dragC = new float[6];
    public float dragRatio, angularDragRatio;
    public float massMomentX = 0, massMomentY = 0, massMomentZ = 0;
    public Crew crew;
    public Leader leader;
    public int totalCost;
    public int totalHP, currentHP;
    public bool isConnected = true;
    public Vector3 velocity = Vector3.zero;             //local velocity
    public Vector3 acceleration = Vector3.zero;         //local acceleration
    public Vector3 moment = Vector3.zero;
    public Vector3 angularVelocity = Vector3.zero;
    int placedMask = 1 << 8;
    public float maxX = 0, minX = 0, maxY = 0, minY = 0, maxZ = 0, minZ = 0;
    float displacement = 0;
    float submergedRatio = 0;
    float airToWater = GlobalVars.waterDensity / GlobalVars.airDensity;
    float deltaTimeRef;
    public int team= 0;
    public bool isPlayer = false;
    Vector3 totalScaled;
    Ocean ocean;
    GameObject selectIcon;
    public bool isTeam = true, isShip = true, isPlane = false;
    public List<Turret> turrets = new List<Turret>();
    public List<Command> commands = new List<Command>();
    Vector3 waypoint = Vector3.zero;
    public bool isFrozen = true;
    public float scanRange = 0;
    Vehicle[] vehicles;
    List<Vehicle> targets = new List<Vehicle>();
    public List<Rooms> rooms = new List<Rooms>();
    public int ammoCapacity = 0, cargoCapacity = 0, crewCapacity = 0, engineCapacity = 0; //0 - empty | 1 - crew | 2 - boiler | 3 - ammo | 4 - cargo
    public float planeCapacity = 0;
    public bool inBattle = false;
    CombatUnitManager cmu;
    MeshCombiner meshCombiner;

    void Awake()
    {
        selectIcon = transform.GetChild(0).gameObject;
        weight = ScriptableObject.CreateInstance<ForceVector>();
        buoyancy = ScriptableObject.CreateInstance<ForceVector>();
        //drag = ScriptableObject.CreateInstance<ForceVector>();
        engineForces = ScriptableObject.CreateInstance<ForceVector>();
        rudderForces = ScriptableObject.CreateInstance<ForceVector>();
        meshCombiner = FindObjectOfType<MeshCombiner>();
        foreach (Transform child in transform)
        {
            if(child.tag == "SelectIcon")
            {
                selectIcon = child.gameObject;
            }
        }

        selectIcon.SetActive(false);
    }

    void Start()
    {
        NewCenterOfMass();
        SetTotalVolume();
        SetMomentsOfInertia();
        UpdateAngularDrag();
        GetAirDrag();

        engineForces.force = Vector3.zero;
        rudderForces.force = Vector3.zero;
        ocean = Ocean.Singleton;


        if (inBattle)
        {
            cmu = FindObjectOfType<CombatUnitManager>();
            Timing.RunCoroutine(TargetScanTimer());
        }
    }

    void Update()
    {
        deltaTimeRef = Time.deltaTime;
        acceleration = Vector3.zero;
        moment = Vector3.zero;

        //UpdateVectors();    //update force and moment vectors
        if (isFrozen)
            velocity = Vector3.zero;
        if(!isFrozen)
        {
            UpdateCenterofBuoyancy();  //update buoyancy force
            buoyancy.Init(centerOfBuoy - centerOfMass, new Vector3(0, totalDisplacement * GlobalVars.waterDensity * GlobalVars.gravity, 0));
            SumForces();

            //Debug.DrawRay(transform.TransformPoint(centerOfDrag), transform.TransformVector(GetDragMoment()), Color.red);
            //Debug.DrawRay(transform.TransformPoint(engineForces.position), transform.TransformVector(engineForces.GetMoment()), Color.blue);
            //Debug.DrawRay(transform.TransformPoint(centerOfMass), dragForce);
            //linear acceleration
            acceleration += weight.force / totalMass;           //start with weight
            acceleration += buoyancy.force / totalMass;         //add buoyancy
            acceleration += transform.TransformVector(engineForces.force) / totalMass;     //add thrusters
            acceleration += transform.TransformVector(rudderForces.force) / totalMass;     //add rudders

            //transform.Rotate(Vector3.up, rudderForces.force.magnitude/totalMass/100*-rudder);
            //acceleration += drag.force / totalMass;    //add drag
            /*for(int i=2; i<forces.Count; i++)
            acceleration += transform.TransformDirection(forces[i].force) / totalMass;*/

            //apply acceleration to velocity
            velocity = velocity + (acceleration * deltaTimeRef);
            //apply drag to velocity
            UpdateDrag();
            dragAcceleration = dragForce / totalMass;
            if (dragAcceleration.magnitude * deltaTimeRef > velocity.magnitude)
                velocity = Vector3.zero;
            else if (dragAcceleration.magnitude > 0)
            {
                dragRatio = (velocity.magnitude - (dragAcceleration.magnitude * deltaTimeRef)) / velocity.magnitude;
                velocity = velocity * dragRatio;
            }
            //apply velocity to position
            transform.Translate(velocity * deltaTimeRef, Space.World);

            //reportDrag = drag.force;
            reportBuoyancy = buoyancy.force;

            //update moments
            UpdateMoments();

            //apply moment vector to velocity vector

            //Debug.Log("Thrust: " + engineForces.GetMoment() + " Drag: " + GetDragMoment() + " Buoyancy: " + buoyancy.GetMoment());

            if (!(float.IsNaN(Vector3.Dot(moment, transform.forward) / massMomentZ) || float.IsNaN(Vector3.Dot(moment, transform.up) / massMomentY) || float.IsNaN(Vector3.Dot(moment, transform.right) / massMomentZ)))
            {
                angularVelocity.z += Vector3.Dot(moment, Vector3.forward) * deltaTimeRef / massMomentZ;
                angularVelocity.y += Vector3.Dot(moment, Vector3.up) * deltaTimeRef / massMomentY;
                angularVelocity.x += Vector3.Dot(moment, Vector3.right) * deltaTimeRef / massMomentX;
            }
            
            UpdateAngularDrag();
            //apply angular drag to angular velocity
            //Debug.Log(angularDrag);
            //Debug.Log(angularDrag.magnitude * angularVelocity.magnitude * angularVelocity.magnitude * deltaTimeRef);
            
            if (!float.IsNaN(angularVelocity.magnitude))
            {
                
                /*
                if (angularDrag.magnitude * angularVelocity.magnitude * angularVelocity.magnitude * deltaTimeRef > angularVelocity.magnitude)
                    angularVelocity = Vector3.zero;
                else if ((angularDragRatio > 0) && angularDragRatio < 1)
                {
                    angularDragRatio = 1 -(angularDrag.magnitude * angularVelocity.magnitude * deltaTimeRef);
                    //Debug.Log(angularDrag.magnitude + " " + angularVelocity.magnitude);
                    angularVelocity = angularVelocity * angularDragRatio;
                }
                */
            }

            angularDragRatio = 1 - (angularDrag.magnitude * angularVelocity.magnitude * deltaTimeRef);
            angularVelocity *= angularDragRatio;


            //rotate about the angular velocity vector
            transform.RotateAround(transform.TransformPoint(centerOfMass), transform.TransformVector(angularVelocity), angularVelocity.magnitude * deltaTimeRef);
        }

        /*
        if (Input.GetKeyDown(KeyCode.T))
        {
            FindObjectOfType<MeshCombiner>().CombineMeshesMultiMat(GetComponent<Vehicle>());
        }
        */

        if(Input.GetKeyDown(KeyCode.F)) //temporary, unfreezing should turn off BC and allow ship to float, re-freezing will lock the boat back into place
        {
            if (isFrozen)
                isFrozen = false;
            else
            {
                isFrozen = true;
                transform.position = new Vector3(0, 20, 0);
                transform.rotation = Quaternion.Euler(Vector3.zero);
            }
        }
        

        if(!isPlayer)
        {
            if (commands.Count > 0)
            {
                waypoint = transform.InverseTransformPoint(commands[0].position);
                rudder = -Mathf.Sign(waypoint.x);
                throttle = 1;
            }
            else
            {
                rudder = 0;
                throttle = 0;
            }
        }
        if(selectIcon.activeInHierarchy)
        {
            selectIcon.transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y,2,100), transform.position.z);
            selectIcon.transform.rotation = Quaternion.Euler(Vector3.right*90);
        }
    }

    Vehicle target;

    IEnumerator<float> TargetScanTimer()
    {
        yield return Timing.WaitForSeconds(Random.Range(0f, 1f) + 1);
        TargetScan();
        Timing.RunCoroutine(TargetScanTimer());
    }

    public void TargetScan()
    {
        if(!target)
        {
            if (isTeam)
                targets = cmu.GetEnemyVehicles();
            else
                targets = cmu.GetAlliedVehicles();
            if(targets.Count>0)
            {
                target = targets[Random.Range(0, targets.Count)];
                for (int i = 0; i < turrets.Count; i++)
                {
                    turrets[i].SetTarget(target.gameObject); //change to use priority targeting
                }
            }
        }
    }

    public void Select(bool isSelected)
    {
        if (isSelected)
            selectIcon.SetActive(true);
        else
            selectIcon.SetActive(false);
        //selectIcon.transform.localScale = GetDimensions()*5;
    }

    void SetTarget(GameObject target)
    {
        if(target == null)
        {
            for (int i = 0; i < turrets.Count; i++)
            {
                turrets[i].target = null;
                turrets[i].hasTarget = false; //replace with target null check
            }
        }
        else
        {
            for (int i = 0; i < turrets.Count; i++)
            {
                turrets[i].target = target;
                turrets[i].hasTarget = true; //replace with target null check
            }
        }
    }

    void FindTurrets()
    {
        turrets.Clear();
        for(int i=0;i<blocks.Count;i++)
        {
            if(blocks[i].GetComponent<Turret>())
            {
                turrets.Add(blocks[i].GetComponent<Turret>());
            }
        }
    }

    /*void UpdateVectors()
    {
        //we need the weight vector, drag vector, sum of applied forces(engines, lift and such), buoyancy vector
        //weight vector is in the UpdateCenterOfMass method
        //buoyancy vector
        UpdateCenterofBuoyancy();
        buoyancy.Init(centerOfBuoy - centerOfMass, new Vector3(0, totalDisplacement * GlobalVars.waterDensity * GlobalVars.gravity, 0));
        //drag vector
        UpdateDrag();

        //sum of applied forces
        //fffffffffffffffffffffffffffffffffffffffffffffffff have a for loop in there somewhere
        //for real though add a Wing and Engine class that inherit Block and that we look for in the Blocks List
        //make sure they are global vectors
        //do this all in SumForces down below

        //update moments
        UpdateMoments();
    }*/

    public void UpdateOpenFaces()
    {
        for(int i=0;i<blocks.Count;i++)
        {
            blocks[i].UpdateOpenFaces();

        }
    }

    void SumForces()
    {
        maxThrust = 0;
        maxRudder = 0;
        totalScaled = Vector3.zero;
        Vector3 rudderScaled = Vector3.zero;

        for (int i = 0; i < blocks.Count; i++)
        {
            if (blocks[i].isThruster)
            {
                maxThrust += blocks[i].GetComponent<Thruster>().thrust;
                totalScaled += blocks[i].GetComponent<Thruster>().thrust * blocks[i].transform.localPosition;
            }
            if (blocks[i].isRudder)
            {
                maxRudder += blocks[i].GetComponent<Rudder>().force;
                rudderScaled += blocks[i].GetComponent<Rudder>().force * blocks[i].transform.localPosition;
            }
        }

        if (maxThrust != 0)
            totalScaled = totalScaled / maxThrust;
        else if(maxThrust == 0)
            totalScaled = centerOfMass;
        if (maxRudder != 0)
            rudderScaled = rudderScaled / maxRudder;
        else if (maxRudder == 0)
            rudderScaled = centerOfMass;
        engineForces.Init(totalScaled - centerOfMass, new Vector3(0, 0, maxThrust * throttle));
        centerOfThrust = totalScaled;
        rudderForces.Init(rudderScaled - centerOfMass, new Vector3(maxRudder * rudder, 0, 0));
    }

    public void UpdateAngularDrag()
    {
        angularDrag = Vector3.zero;
        Vector3 blockDrag = Vector3.zero;

        for (int i = 0; i < blocks.Count; i++)
        {
            //blocks[i].UpdateOpenFaces();
            blockDrag = Vector3.zero;

            //x axis of rotation
            if (blocks[i].openFaces[5])
                blockDrag.x += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.y - centerOfMass.y, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.z - centerOfMass.z, 2), 0.25f)),1.5f);
            if (blocks[i].openFaces[4])
                blockDrag.x += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.y - centerOfMass.y, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.z - centerOfMass.z, 2), 0.25f)),1.5f);
            //y axis of rotation
            if (blocks[i].openFaces[3])
                blockDrag.y += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.x - centerOfMass.x, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.z - centerOfMass.z, 2), 0.25f)),1.5f);
            if (blocks[i].openFaces[2])
                blockDrag.y += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.x - centerOfMass.x, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.z - centerOfMass.z, 2), 0.25f)),1.5f);
            //z axis of rotation
            if (blocks[i].openFaces[1])
                blockDrag.z += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.x - centerOfMass.x, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.y - centerOfMass.y, 2), 0.25f)),1.5f);
            if (blocks[i].openFaces[0])
                blockDrag.z += blocks[i].blockChild.Count * Mathf.Pow((Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.x - centerOfMass.x, 2), 0.25f) + Mathf.Max(Mathf.Pow(blocks[i].transform.localPosition.y - centerOfMass.y, 2), 0.25f)),1.5f);
            if (blocks[i].transform.position.y < 0)
                blockDrag *= airToWater;
            angularDrag += blockDrag;
        }
        angularDrag *= (GlobalVars.airDensity * Mathf.PI) / 180;

        if (!float.IsNaN(massMomentX) && !float.IsNaN(massMomentY) && !float.IsNaN(massMomentZ))
        {
            angularDrag.x /= massMomentX;
            angularDrag.y /= massMomentY;
            angularDrag.z /= massMomentZ;
        }
    }

    public void GetAirDrag()
    {
        float[] maximums = GetMaxDimensions();
        float temp;
        RaycastHit hit;
        Vector3 normal;

        for (int i = 0; i < 6; i++)
        {
            dragC[i] = 0;
        }
        dragXMinus = Vector3.zero;
        dragXPlus = Vector3.zero;
        dragYMinus = Vector3.zero;
        dragYPlus = Vector3.zero;
        dragZMinus = Vector3.zero;
        dragZPlus = Vector3.zero;

        for (int i = 0; i < blocks.Count; i++)
        {
            for (int x = 0; x < blocks[i].blockChild.Count; x++)
            {
                blocks[i].blockChild[x].gameObject.SetActive(false);
            }
            blocks[i].GetComponent<MeshCollider>().enabled = true;
        }
        for (float x = maximums[3]-1; x <= maximums[2]+1; x++)
        {
            for (float y = maximums[5]-1; y <= maximums[4]+1; y++)
            {
                //Debug.DrawRay(new Vector3(x + transform.position.x, y + transform.position.y, maximums[0] + 10 + transform.position.z), new Vector3(0, 0, -1), Color.red, Mathf.Infinity);
                if (Physics.Raycast(new Vector3(x + transform.position.x, y + transform.position.y, maximums[0] + 10 + transform.position.z), new Vector3(0, 0, -1), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(0, 0, 1), normal));
                    hit.transform.GetComponent<Block>().baseDrag[0] = temp;
                    dragZPlus += temp * hit.transform.localPosition;
                    dragC[0] += temp;
                }
                //Debug.DrawRay(new Vector3(x + transform.position.x, y + transform.position.y, maximums[1] - 10 + transform.position.z), new Vector3(0, 0, 1), Color.red, Mathf.Infinity);

                if (Physics.Raycast(new Vector3(x + transform.position.x, y + transform.position.y, maximums[1] - 10 + transform.position.z), new Vector3(0, 0, 1), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(0, 0, -1), normal));
                    hit.transform.GetComponent<Block>().baseDrag[1] = temp;
                    dragZMinus += temp * hit.transform.localPosition;
                    dragC[1] += temp;
                }
            }
        }

        for (float z = maximums[1] - 1; z <= maximums[0] + 1; z++)
        {
            for (float y = maximums[5] - 1; y <= maximums[4] + 1; y++)
            {
                //Debug.DrawRay(new Vector3(maximums[2] + 10 + transform.position.x, y + transform.position.y, z + transform.position.z), new Vector3(-1, 0, 0), Color.green, Mathf.Infinity);

                if (Physics.Raycast(new Vector3(maximums[2] + 10 + transform.position.x, y + transform.position.y, z + transform.position.z), new Vector3(-1, 0, 0), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(1, 0, 0), normal));
                    hit.transform.GetComponent<Block>().baseDrag[2] = temp;
                    dragXPlus += temp * hit.transform.localPosition;
                    dragC[2] += temp;
                }
                //Debug.DrawRay(new Vector3(maximums[3] - 10 + transform.position.x, y + transform.position.y, z + transform.position.z), new Vector3(1, 0, 0), Color.green, Mathf.Infinity);

                if (Physics.Raycast(new Vector3(maximums[3] - 10 + transform.position.x, y + transform.position.y, z + transform.position.z), new Vector3(1, 0, 0), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(-1, 0, 0), normal));
                    hit.transform.GetComponent<Block>().baseDrag[3] = temp;
                    dragXMinus += temp * hit.transform.localPosition;
                    dragC[3] += temp;
                }
            }
        }

        for (float x = maximums[3] - 1; x <= maximums[2] + 1; x++)
        {
            for (float z = maximums[1] - 1; z <= maximums[0] + 1; z++)
            {
                //Debug.DrawRay(new Vector3(x + transform.position.x, maximums[4] + 10 + transform.position.y, z + transform.position.z), new Vector3(0, -1, 0), Color.blue, Mathf.Infinity);

                if (Physics.Raycast(new Vector3(x + transform.position.x, maximums[4] + 10 + transform.position.y, z + transform.position.z), new Vector3(0, -1, 0), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(0, 1, 0), normal));
                    hit.transform.GetComponent<Block>().baseDrag[4] = temp;
                    dragYPlus += temp * hit.transform.localPosition;
                    dragC[4] += temp;
                }
                //Debug.DrawRay(new Vector3(x + transform.position.x, maximums[5] - 10 + transform.position.y, z + transform.position.z), new Vector3(0, 1, 0), Color.blue, Mathf.Infinity);

                if (Physics.Raycast(new Vector3(x + transform.position.x, maximums[5] - 10 + transform.position.y, z + transform.position.z), new Vector3(0, 1, 0), out hit, Mathf.Infinity, placedMask))
                {
                    normal = hit.normal;

                    temp = Mathf.Cos(Vector3.Angle(new Vector3(0, -1, 0), normal));
                    hit.transform.GetComponent<Block>().baseDrag[5] = temp;
                    dragYMinus += temp * hit.transform.localPosition;
                    dragC[5] += temp;
                }
            }
        }

        dragXPlus /= dragC[2];
        dragXMinus /= dragC[3];
        dragYPlus /= dragC[4];
        dragYMinus /= dragC[5];
        dragZPlus /= dragC[0];
        dragZMinus /= dragC[1];

        for (int i = 0; i < 6; i++)
        {
            dragC[i] *= GlobalVars.airDensity / 2;
        }

        
        for (int i = 0; i < blocks.Count; i++)
        {
            for (int x = 0; x < blocks[i].blockChild.Count; x++)
            {
                blocks[i].blockChild[x].gameObject.SetActive(true);
            }
            blocks[i].GetComponent<MeshCollider>().enabled = false;
        }
    }

    public void UpdateDrag()
    {
        Quaternion angles = Quaternion.Euler(transform.InverseTransformVector(velocity));
        Vector3 temp = Vector3.zero;
        Vector3 tempPos = Vector3.zero;
        float CosZ = -Mathf.Cos(Mathf.Deg2Rad * angles.z);
        float CosY = -Mathf.Cos(Mathf.Deg2Rad * angles.y);
        float CosX = -Mathf.Cos(Mathf.Deg2Rad * angles.x);
        float zSqr = transform.InverseTransformVector(velocity).z * transform.InverseTransformVector(velocity).z;
        float xSqr = transform.InverseTransformVector(velocity).x * transform.InverseTransformVector(velocity).x;
        float ySqr = transform.InverseTransformVector(velocity).y * transform.InverseTransformVector(velocity).y;

        centerOfDrag = Vector3.zero;
        if (angles.z < 90 || angles.z > 270)
        {
            dragForce.z = CosZ * dragC[0] * zSqr;
            centerOfDrag += Mathf.Abs(dragForce.z) * dragZPlus;
        }
        if (angles.z > 90 && angles.z < 270)
        {
            dragForce.z = CosZ * dragC[1] * zSqr;
            centerOfDrag += Mathf.Abs(dragForce.z) * dragZMinus;
        }
        if (angles.x < 90 || angles.x > 270)
        {
            dragForce.x = CosX * dragC[2] * xSqr;
            centerOfDrag += Mathf.Abs(dragForce.x) * dragXPlus;
        }
        if (angles.x > 90 && angles.x < 270)
        {
            dragForce.x = CosX * dragC[3] * xSqr;
            centerOfDrag += Mathf.Abs(dragForce.x) * dragXMinus;
        }
        if (angles.y < 90 || angles.y > 270)
        {
            dragForce.y = CosY * dragC[4] * ySqr;
            centerOfDrag += Mathf.Abs(dragForce.y) * dragYPlus;
        }
        if (angles.y > 90 && angles.y < 270)
        {
            dragForce.y = CosY * dragC[5] * ySqr;
            centerOfDrag += Mathf.Abs(dragForce.y) * dragYMinus;
        }

        for(int i = 0; i < blocks.Count; i++)
        {
            if(blocks[i].transform.position.y < 0 && blocks[i].isSurface)
            {
                if (angles.z < 90 || angles.z > 270)
                {
                    temp.z = CosZ * blocks[i].baseDrag[0] * zSqr / 2;
                    tempPos = Mathf.Abs(temp.z) * blocks[i].transform.localPosition;
                }
                if (angles.z > 90 && angles.z < 270)
                {
                    temp.z = CosZ * blocks[i].baseDrag[1] * zSqr / 2;
                    tempPos = Mathf.Abs(temp.z) * blocks[i].transform.localPosition;
                }
                if (angles.x < 90 || angles.x > 270)
                {
                    temp.x = CosX * blocks[i].baseDrag[2] * xSqr / 2;
                    tempPos = Mathf.Abs(temp.x) * blocks[i].transform.localPosition;
                }
                if (angles.x > 90 && angles.x < 270)
                {
                    temp.x = CosX * blocks[i].baseDrag[3] * xSqr / 2;
                    tempPos = Mathf.Abs(temp.x) * blocks[i].transform.localPosition;
                }
                if (angles.y < 90 || angles.y > 270)
                {
                    temp.y = CosY * blocks[i].baseDrag[4] * ySqr / 2;
                    tempPos = Mathf.Abs(temp.y) * blocks[i].transform.localPosition;
                }
                if (angles.y > 90 && angles.y < 270)
                {
                    temp.y = CosY * blocks[i].baseDrag[5] * ySqr / 2;
                    tempPos = Mathf.Abs(temp.y) * blocks[i].transform.localPosition;
                }

                dragForce += temp;
                centerOfDrag += tempPos;
            }
        }
        centerOfDrag /= dragForce.magnitude;
    }

    public Vector3 GetDimensions()
    {
        FindObjectOfType<MeshCombiner>().CombineMeshes(gameObject);
        Vector3 dimension = GetComponent<Renderer>().bounds.size;
        GetComponent<MeshFilter>().mesh = null;
        for(int i=0;i<blocks.Count;i++)
        {
            blocks[i].mesh.SetActive(true);
        }
        return dimension;
    }

    public float[] GetMaxDimensions()
    {
        float[] dimensions = new float[6];
        for (int i = 0; i < 6; i++)
        {
            dimensions[i] = 0;
        }

        for (int i = 0; i < blocks.Count; i++)
        {
            for (int x = 0; x < blocks[i].blockChild.Count; x++)
            {
                if (blocks[i].blockChild[x].transform.position.z - transform.position.z > dimensions[0])
                {
                    dimensions[0] = blocks[i].blockChild[x].transform.position.z - transform.position.z;
                }
                if (blocks[i].blockChild[x].transform.position.z - transform.position.z < dimensions[1])
                {
                    dimensions[1] = blocks[i].blockChild[x].transform.position.z - transform.position.z;
                }
                if (blocks[i].blockChild[x].transform.position.x - transform.position.x > dimensions[2])
                {
                    dimensions[2] = blocks[i].blockChild[x].transform.position.x - transform.position.x;
                }
                if (blocks[i].blockChild[x].transform.position.x - transform.position.x < dimensions[3])
                {
                    dimensions[3] = blocks[i].blockChild[x].transform.position.x - transform.position.x;
                }
                if (blocks[i].blockChild[x].transform.position.y - transform.position.y > dimensions[4])
                {
                    dimensions[4] = blocks[i].blockChild[x].transform.position.y - transform.position.y;
                }
                if (blocks[i].blockChild[x].transform.position.y - transform.position.y < dimensions[5])
                {
                    dimensions[5] = blocks[i].blockChild[x].transform.position.y - transform.position.y;
                }
            }
        }
        maxZ = dimensions[0];
        minZ = dimensions[1];
        maxX = dimensions[2];
        minX = dimensions[3];
        maxY = dimensions[4];
        minY = dimensions[5];
        return dimensions;
    }

    public void NewCenterOfMass()
    {
        totalMass = 0;
        totalScaled = Vector3.zero;

        for (int i=0; i < blocks.Count; i++)
        {
            totalMass += blocks[i].mass;
            totalScaled += blocks[i].transform.localPosition * blocks[i].mass;
        }

        centerOfMass = totalScaled / totalMass;
        wt = totalMass * GlobalVars.gravity;
        weight.Init(centerOfMass, new Vector3(0, -wt));
    }

    public void SetMomentsOfInertia()
    {
        massMomentX = 0;
        massMomentY = 0;
        massMomentZ = 0;
        float xAxisDist, yAxisDist, zAxisDist, mass;
        float x, y, z;

        for (int i = 0; i < blocks.Count; i++)
        {
            x = blocks[i].transform.localPosition.x;
            y = blocks[i].transform.localPosition.y;
            z = blocks[i].transform.localPosition.z;
            mass = blocks[i].mass;

            if (new Vector3 (x,y,z).magnitude < 0.5)
            {
                massMomentX += mass / 6;
                massMomentY = massMomentX;
                massMomentZ = massMomentX;
            }

            xAxisDist = Mathf.Sqrt(y * y + z * z);
            yAxisDist = Mathf.Sqrt(x * x + z * z);
            zAxisDist = Mathf.Sqrt(x * x + y * y);

            massMomentX += mass * xAxisDist * xAxisDist;
            massMomentY += mass * yAxisDist * yAxisDist;
            massMomentZ += mass * zAxisDist * zAxisDist;
        }
    }

    public void SetTotalVolume()
    {
        totalVolume = 0;
        for (int i=0; i < blocks.Count; i++)
            totalVolume += blocks[i].volume;
    }

    float localHeight;
    public void UpdateCenterofBuoyancy()
    {
        totalDisplacement = 0;
        displacement = 0;
        submergedRatio = 0;
        localHeight = 0.5f;
        totalScaled = Vector3.zero;

        for (int i = 0; i < blocks.Count; i++)
        {
            if (!(blocks[i].isRoom) && !(blocks[i].isTurret))
            {
                for(int x=0;x<blocks[i].blockChild.Count;x++)
                {
                    //localHeight = ocean.GetHeightChoppyAtLocation2(blocks[i].transform.position.x, blocks[i].transform.position.z);
                    if (blocks[i].blockChild[x].transform.position.y < localHeight)
                    {
                        if (blocks[i].blockChild[x].transform.position.y > localHeight - 1)
                            submergedRatio = localHeight - blocks[i].blockChild[x].transform.position.y;
                        else
                            submergedRatio = 1;
                        //Debug.Log(submergedRatio + " " + (blocks[i].volume) +"|"+ blocks[i].blockChild.Count);
                        displacement = submergedRatio * blocks[i].volume / (blocks[i].transform.childCount - 1);
                        totalDisplacement += displacement;
                        totalScaled += blocks[i].transform.localPosition * displacement;
                    }
                }

            }
            else if (blocks[i].isRoom)
            {
                //totalDisplacement += blocks[i].displacement;
                //totalScaled += blocks[i].localPosCache * blocks[i].displacement;
            }
        }

        for (int i = 0; i < rooms.Count; i++)
        {
            totalDisplacement += rooms[i].displacement;
            totalScaled += rooms[i].centroid * rooms[i].displacement;
        }

        //Debug.Log(totalScaled + " " + totalDisplacement);
        if (totalDisplacement == 0)
            centerOfBuoy = centerOfMass;
        else
            centerOfBuoy = totalScaled / totalDisplacement;
    }

    public void UpdateCenterOfMass(int i)
    {
        centerOfMass = (centerOfMass * totalMass - blocks[i].mass * blocks[i].localPosCache) / (totalMass - blocks[i].mass);
        totalMass -= blocks[i].mass;
        wt = totalMass * GlobalVars.gravity;
        weight.Init(centerOfMass, new Vector3(0, -wt, 0));
    }

    /*public void UpdateDrag()
    {
        //drag.force = Vector3.zero;
        centerOfDrag = Vector3.zero;
        dragCoefficient = Vector3.zero;
        Vector3 cDrag = Vector3.zero;
        Vector3 dragPos = Vector3.zero;

        for (int i = 0; i < blocks.Count; i++)
        {
            if (blocks[i].isSurface)
            {
                blocks[i].UpdateDrag(transform.InverseTransformVector(velocity));
                for(int x=0;x<blocks[i].blockChild.Count;x++)
                {
                    cDrag = transform.TransformVector(blocks[i].CDrag) / (blocks[i].blockChild.Count);
                    dragPos = blocks[i].CDrag.magnitude * blocks[i].transform.localPosition / (blocks[i].blockChild.Count);
                    if (blocks[i].blockChild[x].transform.position.y < 0.5f)
                    {
                        if (blocks[i].blockChild[x].transform.position.y > -0.5f)
                            submergedRatio = 1 - (blocks[i].blockChild[x].transform.position.y + 0.5f);
                        else
                            submergedRatio = 1;
                        cDrag *= (submergedRatio * airToWater);
                        dragPos *= (submergedRatio * airToWater);
                    }
                    dragCoefficient += cDrag;
                    centerOfDrag += dragPos;
                }
            }
        }
        if (dragCoefficient.magnitude != 0)
            centerOfDrag = centerOfDrag / dragCoefficient.magnitude;
        //drag.position = centerOfDrag - centerOfMass;
        dragForce = Vector3.Dot(dragCoefficient, velocity) * velocity;
    }*/

    public void UpdateMoments()
    {
        //Debug.Log(GetDragMoment() + "|" + rudderForces.GetMoment());
        moment = buoyancy.GetMoment() + engineForces.GetMoment() + rudderForces.GetMoment()*200;
    }

    public Vector3 GetDragMoment()
    {
        //Vector3 dragMoment = Vector3.Cross(centerOfDrag - centerOfMass, transform.InverseTransformVector(dragForce));
        //Vector3 dragMoment = (centerOfDrag-centerOfMass).magnitude * transform.InverseTransformVector(dragForce).magnitude * Mathf.Sin(Vector3.Angle((centerOfDrag - centerOfMass), transform.InverseTransformVector(dragForce))) * Vector3.Cross(Vector3.up, (centerOfDrag - centerOfMass)- transform.InverseTransformVector(dragForce)).normalized;
        Vector3 dragMoment = Vector3.zero;
        dragMoment.x += dragForce.y * (centerOfDrag.z - centerOfMass.z);
        dragMoment.x += -dragForce.z * (centerOfDrag.y - centerOfMass.y);
        dragMoment.y += -dragForce.x * (centerOfDrag.z - centerOfMass.z);
        dragMoment.y += dragForce.z * (centerOfDrag.x - centerOfMass.x);
        dragMoment.z += dragForce.x * (centerOfDrag.y - centerOfMass.y);
        dragMoment.z += -dragForce.y * (centerOfDrag.x - centerOfMass.x);
        //Debug.Log(dragMoment);
        return dragMoment;
    }

    public float GetUnderwaterRatio()
    {
        return 0;
    }

    public void FinalizeColliders()
    {
        for (int i = 0; i <blocks.Count;i++)
        {
            for(int x=0;x<blocks[i].blockChild.Count;x++)
            {
                blocks[i].blockChild[x].GetComponent<BoxCollider>().enabled = false;
            }
            blocks[i].GetComponent<MeshCollider>().enabled = true;
        }
    }

    public void FindTotalCost()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            totalCost += blocks[i].cost;
        }
    }

    public void FindTotalHP()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            totalHP += blocks[i].hp;
        }
    }

    /*
    public void FindAdjacentBlocks()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            Collider[] tempCollider;
            for (int x = 0; x < blocks[i].blockChild.Count; x++)
            {
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.up, Vector3.one / 3, transform.rotation, placedMask);
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.up, Vector3.one / 3, transform.rotation, placedMask);
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.right, Vector3.one / 3, transform.rotation, placedMask);
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.right, Vector3.one / 3, transform.rotation, placedMask);
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.forward, Vector3.one / 3, transform.rotation, placedMask);
                tempCollider = Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.forward, Vector3.one / 3, transform.rotation, placedMask);
            }
        }
    }
    */

    public void DetectSurfaceBlocks()
    {
        for(int i=0;i<blocks.Count;i++)
        {
            blocks[i].isSurface = true;
            blocks[i].mesh.SetActive(true);
            int tempCounter = 0;
            int goal=0;
            for(int x=0;x<blocks[i].blockChild.Count;x++)
            {
                goal++;
                if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.up, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                    if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.up, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                        if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.right, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                            if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.right, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                                if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position + blocks[i].blockChild[x].transform.forward, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                                    if (Physics.OverlapBox(blocks[i].blockChild[x].transform.position - blocks[i].blockChild[x].transform.forward, Vector3.one / 3, transform.rotation, placedMask).Length >= 1)
                                        tempCounter++;
            }
            if (tempCounter == goal)
            {
                blocks[i].isSurface = false;
                blocks[i].mesh.SetActive(false);
            }
        }
    }

    public void RenumberParts()
    {
        for(int i=0;i<blocks.Count;i++)
        {
            blocks[i].placeID = i;
        }
    }

    public void RoomCombiner(int i)
    {
        SetChecked();
        blocks[i].isChecked = true;
        RoomCheck(i);
        Rooms temp = null;
        for (int x = 0; x < blocks.Count; x++)
        {
            if (blocks[x].isChecked &&blocks[x].room != null)
            {
                temp = blocks[x].room;
                break;
            }
        }
        if (temp == null)
        {
            temp = new Rooms();
            temp.rid = rooms.Count;
            rooms.Add(temp);
        }
        for (int x = 0; x < blocks.Count; x++)
        {
            if (blocks[x].isChecked&&blocks[x].isRoom&&blocks[x].room==null)
            {
                blocks[x].room = temp;
                temp.roomBlocks.Add(blocks[x]);
            }
        }
    }

    public void FinalizeShip()
    {
        Timing.RunCoroutine(FinalizeDelay());
    }

    IEnumerator<float> FinalizeDelay()
    {
        yield return Timing.WaitForOneFrame;
        DetectSurfaceBlocks();
        DisconnectTest();
        NewCenterOfMass();
        SetTotalVolume();
        SetMomentsOfInertia();
        UpdateOpenFaces();
        UpdateAngularDrag();
        GetRoomCapacity();
        GetAirDrag();
        meshCombiner.CombineMeshesMultiMat(GetComponent<Vehicle>());
        FinalizeColliders();
        FindTotalCost();
        FindTotalHP();
        FindTurrets();
    }

    public void GetRoomCapacity()
    {
        crewCapacity = 0;
        engineCapacity = 0;
        ammoCapacity = 0;
        cargoCapacity = 0;

        for(int i=0;i<rooms.Count;i++)
        {
            switch(rooms[i].roomType) //0 - empty | 1 - crew | 2 - boiler | 3 - ammo | 4 - cargo | 5 - hanger
            {
                case 0: //Arbitrary values for now, balance later
                    break;
                case 1:
                    crewCapacity += 20;
                    break;
                case 2:
                    engineCapacity += 100;
                    break;
                case 3:
                    ammoCapacity += 100;
                    break;
                case 4:
                    cargoCapacity += 100;
                    break;
                case 5:
                    planeCapacity += 0.25f;
                    break;
                default:
                    Debug.Log("Something broke");
                    break;
            }
        }
        planeCapacity = Mathf.Floor(planeCapacity);
    }

    void RoomCheck(int i)
    {
        foreach (Transform child in blocks[i].transform) //Update to use cached children
        {
            if (child.GetComponent<BoxCollider>())
            {
                RoomChecker(Physics.OverlapBox(child.transform.position + child.transform.up, Vector3.one / 3, transform.rotation, placedMask));
                RoomChecker(Physics.OverlapBox(child.transform.position - child.transform.up, Vector3.one / 3, transform.rotation, placedMask));
                RoomChecker(Physics.OverlapBox(child.transform.position + child.transform.right, Vector3.one / 3, transform.rotation, placedMask));
                RoomChecker(Physics.OverlapBox(child.transform.position - child.transform.right, Vector3.one / 3, transform.rotation, placedMask));
                RoomChecker(Physics.OverlapBox(child.transform.position + child.transform.forward, Vector3.one / 3, transform.rotation, placedMask));
                RoomChecker(Physics.OverlapBox(child.transform.position - child.transform.forward, Vector3.one / 3, transform.rotation, placedMask));
            }
        }
    }

    void RoomChecker(Collider[] temp)
    {
        if (temp.Length >= 1)
        {
            if (!temp[0].transform.parent.GetComponent<Block>().isChecked && temp[0].transform.parent.GetComponent<Block>().isRoom)
            {
                temp[0].transform.parent.GetComponent<Block>().isChecked = true;
                RoomCheck(temp[0].transform.parent.GetComponent<Block>().placeID);
            }
        }
    }

    public void DisconnectTest()
    {
        SetChecked();
        NeighborCheck(0);
        isConnected = true;
        for (int x = 0; x < blocks.Count; x++)
        {
            if (!blocks[x].isChecked)
            {
                blocks[x].transform.GetChild(0).GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 255); //Optimize
                isConnected = false;
            }
            else
            {
                blocks[x].transform.GetChild(0).GetComponent<Renderer>().material.color = blocks[x].defaultColor;
            }
        }
    }

    void NeighborCheck(int i)
    {
        foreach (Transform child in blocks[i].transform) //Update to use cached children
        {
            if (child.GetComponent<BoxCollider>())
            {
                CheckChecker(Physics.OverlapBox(child.transform.position + child.transform.up, Vector3.one / 3, transform.rotation, placedMask));
                CheckChecker(Physics.OverlapBox(child.transform.position - child.transform.up, Vector3.one / 3, transform.rotation, placedMask));
                CheckChecker(Physics.OverlapBox(child.transform.position + child.transform.right, Vector3.one / 3, transform.rotation, placedMask));
                CheckChecker(Physics.OverlapBox(child.transform.position - child.transform.right, Vector3.one / 3, transform.rotation, placedMask));
                CheckChecker(Physics.OverlapBox(child.transform.position + child.transform.forward, Vector3.one / 3, transform.rotation, placedMask));
                CheckChecker(Physics.OverlapBox(child.transform.position - child.transform.forward, Vector3.one / 3, transform.rotation, placedMask));
            }
        }
    }

    void CheckChecker(Collider[] temp)
    {
        if (temp.Length >= 1)
        {
            if (!temp[0].transform.parent.GetComponent<Block>().isChecked)
            {
                temp[0].transform.parent.GetComponent<Block>().isChecked = true;
                NeighborCheck(temp[0].transform.parent.GetComponent<Block>().placeID);
            }
        }
    }

    void SetChecked()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            blocks[i].isChecked = false;
        }
        blocks[0].isChecked = true;
    }
}
