﻿using UnityEngine;
using System.Collections;

public class BuoyancyTest : MonoBehaviour
{
    Ocean ocean;
    float heightDifference;

    // Use this for initialization
    void Start()
    {
        ocean = Ocean.Singleton;
    }

    // Update is called once per frame
    void Update()
    {
        heightDifference = transform.position.y - ocean.GetWaterHeightAtLocation2(transform.position.x, transform.position.z);

        transform.Translate(0, -heightDifference, 0);
    }
}
