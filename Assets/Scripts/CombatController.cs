﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class CombatController : MonoBehaviour
{
    public GameObject newVehicle,loadMenuBGPrefab;
    GameObject mainCamera;
    BlockDictionary blockDict;
    GameObject loadMenuBG, loadMenuContent, messageBG;
    List<GameObject> loadMenuButtonList = new List<GameObject>();
    public GameObject loadShipButton;
    Toggle isTeam;

    void Start()
    {
        loadMenuBG = Instantiate(loadMenuBGPrefab);
        loadMenuBG.transform.SetParent(transform.parent);
        loadMenuBG.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        blockDict = FindObjectOfType<BlockDictionary>();
        mainCamera = GameObject.Find("Main Camera");
        loadMenuContent = GameObject.Find("LoadMenuContent");
        isTeam = GameObject.Find("IsTeamToggle").GetComponent<Toggle>();
        loadMenuBG.SetActive(false);

    }

    public void LoadShipMenu()
    {
        mainCamera.GetComponent<CameraFollow>().locked = true;
        loadMenuBG.SetActive(true);

        for (int i = 0; i < loadMenuButtonList.Count; i++)
        {
            Destroy(loadMenuButtonList[i].gameObject);
        }
        string[] files = Directory.GetFiles("Ships/");
        int offset = -60;
        int counter = 0;
        foreach (string file in files)
        {

            GameObject clone = Instantiate(loadShipButton) as GameObject;
            clone.transform.SetParent(loadMenuContent.transform);
            clone.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, offset, 0);
            clone.name = Path.GetFileName(file);
            clone.transform.GetChild(0).GetComponent<Text>().text = clone.name;
            clone.GetComponent<LoadShipButton>().cc = GetComponent<CombatController>();
            loadMenuButtonList.Add(clone);
            offset -= 60;
            counter++;
        }
        loadMenuContent.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 100 + (counter * 200));
    }

    public void LoadShip(string file)
    {
        GameObject obj = Instantiate(newVehicle, mainCamera.transform.position,Quaternion.Euler(Vector3.zero));
        Vehicle vehicle = obj.GetComponent<Vehicle>();
        string line;
        char[] delimiter = { '|' };

        for (int i = 0; i < vehicle.GetComponent<Vehicle>().GetComponent<Vehicle>().blocks.Count; i++)
        {
            Destroy(vehicle.GetComponent<Vehicle>().blocks[i].gameObject);
        }
        vehicle.GetComponent<Vehicle>().blocks.Clear();

        int tempPlaceID = 0;
        if (!Directory.Exists("Ships/"))
            Directory.CreateDirectory("Ships/");
        StreamReader s = File.OpenText("Ships/" + file);
        line = s.ReadLine();
        string[] tempString = line.Split(delimiter);
        vehicle.GetComponent<Vehicle>().shipName = tempString[0];
        vehicle.GetComponent<Vehicle>().totalCost = int.Parse(tempString[1]);
        vehicle.GetComponent<Vehicle>().centerOfMass = new Vector3(float.Parse(tempString[2]), float.Parse(tempString[3]), float.Parse(tempString[4]));
        vehicle.GetComponent<Vehicle>().totalMass = float.Parse(tempString[5]);
        vehicle.GetComponent<Vehicle>().totalHP = int.Parse(tempString[6]);
        line = s.ReadLine();
        while (line != "#BLOCKSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#TURRETSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            temp.GetComponent<Turret>().turretName = tempString[7];
            temp.GetComponent<Turret>().barrelCount = int.Parse(tempString[8]);
            temp.GetComponent<Turret>().caliber = float.Parse(tempString[9]);
            temp.GetComponent<Turret>().length = float.Parse(tempString[10]);
            temp.GetComponent<Turret>().isAA = bool.Parse(tempString[11]);
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#ROOMSEND#")
        {
            tempString = line.Split(delimiter);
            Rooms temp = new Rooms();
            temp.rid = int.Parse(tempString[0]);
            temp.roomType = int.Parse(tempString[1]);
            vehicle.rooms.Add(temp);
            line = s.ReadLine();
        }
        line = s.ReadLine();
        while (line != "#ROOMBLOCKSEND#")
        {
            tempString = line.Split(delimiter);
            GameObject temp = Instantiate(blockDict.blockDict[int.Parse(tempString[0])]);
            temp.SetActive(true);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = tempPlaceID;
            temp.transform.localPosition = new Vector3(float.Parse(tempString[1]), float.Parse(tempString[2]), float.Parse(tempString[3]));
            temp.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(tempString[4]), float.Parse(tempString[5]), float.Parse(tempString[6])));
            int tempRID = int.Parse(tempString[7]);
            for (int x = 0; x < vehicle.rooms.Count; x++)
            {
                if (tempRID == vehicle.rooms[x].rid)
                {
                    temp.GetComponent<Block>().room = vehicle.rooms[x];
                    vehicle.rooms[x].roomBlocks.Add(temp.GetComponent<Block>());
                }
            }
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.blocks.Add(temp.GetComponent<Block>());
            tempPlaceID++;
            line = s.ReadLine();
        }
        vehicle.inBattle = true;
        vehicle.isTeam = isTeam.isOn;
        vehicle.isFrozen = false;
        vehicle.FinalizeShip();
        s.Close();
        loadMenuBG.SetActive(false);
        mainCamera.GetComponent<CameraFollow>().locked = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void SpawnShips(StoredVehicle spawn,Vector3 shipPos,Quaternion shipRot)
    {
        GameObject vehicle = Instantiate(newVehicle, shipPos, shipRot);
        Vehicle vc = vehicle.GetComponent<Vehicle>();
        vc.shipName = spawn.shipName;
        vc.crew = spawn.crew;

        for (int i=0;i<spawn.blocks.Count;i++)
        {
            GameObject temp = Instantiate(blockDict.blockDict[spawn.blocks[i].id]);
            temp.transform.SetParent(vehicle.transform);
            temp.GetComponent<Block>().placeID = i;
            temp.GetComponent<Block>().hp = spawn.blocks[i].hp;
            temp.transform.localPosition = new Vector3(spawn.blocks[i].x,spawn.blocks[i].y,spawn.blocks[i].z);
            temp.transform.rotation = Quaternion.Euler(new Vector3(spawn.blocks[i].rotX,spawn.blocks[i].rotY,spawn.blocks[i].z));
            temp.layer = 8;
            foreach (Transform child in temp.transform)
            {
                if (child.GetComponent<BoxCollider>())
                {
                    child.gameObject.layer = 8;
                }
            }
            vehicle.GetComponent<Vehicle>().blocks.Add(temp.GetComponent<Block>());
        }
    }
}
