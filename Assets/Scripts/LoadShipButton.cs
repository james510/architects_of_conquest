﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadShipButton : MonoBehaviour
{
    public BuildController bc;
    public CombatController cc;
	// Use this for initialization
	void Start ()
    {
        GetComponent<Button>().onClick.AddListener(LoadShip);
    }

    // Update is called once per frame
    void LoadShip ()
    {
        if (bc)
            bc.LoadShip(gameObject.name);
        else if (cc)
            cc.LoadShip(gameObject.name);
	}
}
