﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    public float friction = .95F;
    public float gravity = .1f;
    float rotationY = 1F;
    public float speed;

    public bool locked = false;
    public bool rtsMode = false;

    void Awake ()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if(!locked)
        {
            if(!rtsMode || rtsMode && Input.GetMouseButtonDown(1))
            {
                if(rtsMode)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                }
                if (axes == RotationAxes.MouseXAndY) //Mouse movement translating to body rotation and head rotation
                {
                    float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
                    rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                    rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
                    transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
                }
                else if (axes == RotationAxes.MouseX)
                {
                    transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
                }
                else
                {
                    rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                    rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
                    transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
                }
            }
            if(rtsMode && Input.GetMouseButtonUp(1))
                Cursor.lockState = CursorLockMode.None;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(locked)
            {
                locked = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                locked = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (Input.GetKey(KeyCode.LeftShift))
            speed = 60;
        else
            speed = 20;
        if(!Input.GetKey(KeyCode.Tab))
        {
            if (Input.GetKey(KeyCode.W))
                transform.Translate((Vector3.forward * speed) * Time.deltaTime);
            if (Input.GetKey(KeyCode.S))
                transform.Translate((-Vector3.forward * speed) * Time.deltaTime);
            if (Input.GetKey(KeyCode.D))
                transform.Translate((Vector3.right * speed) * Time.deltaTime);
            if (Input.GetKey(KeyCode.A))
                transform.Translate((-Vector3.right * speed) * Time.deltaTime);
            if (Input.GetKey(KeyCode.Space))
                transform.Translate((Vector3.up * speed) * Time.deltaTime);
            if (Input.GetKey(KeyCode.LeftAlt))
                transform.Translate((-Vector3.up * speed) * Time.deltaTime);
        }
    }
}
