﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command : ScriptableObject
{
    public Vector3 position;
    public Quaternion rotation;
    public int commandType = 0;
}
