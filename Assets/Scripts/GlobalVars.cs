﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVars
{
    public static float gravity = 9.81f;       //m/s^2
    public static float waterDensity = 1;    //kg/m^3
    public static float airDensity = .001225f;
};