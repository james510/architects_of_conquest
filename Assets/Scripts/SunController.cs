﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunController : MonoBehaviour
{
    public float speed = 0;
    private Light sun;

    void Awake()
    {
        sun = transform.GetChild(0).GetComponent<Light>();
    }
    // Update is called once per frame

    void Update ()
    {
        sun.intensity = Mathf.Abs(transform.rotation.x);
        transform.Rotate(Vector3.right*Time.deltaTime*speed);
	}
}
