﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    int frameCount = 0;
    float dt = 0.0f;
    float fps = 0.0f;
    float updateRate = 4.0f;
    float benchmark = 60;
    int benchmarkCount=1;
    public GameObject fpsTextObject;
    Text fpsText;
	// Use this for initialization
	void Start ()
    {
        fpsText = fpsTextObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        frameCount++;
        dt += Time.deltaTime;
        if(dt>1.0/updateRate)
        {
            benchmarkCount++;
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;
            benchmark = fps + benchmark;
        }
        fpsText.text = "FPS: " + fps.ToString("F0") + "\nBenchmark Average: " + (benchmark/benchmarkCount).ToString("F0");
	}
}
