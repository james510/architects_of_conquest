﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turret : MonoBehaviour
{
    public Vector3 invTargetPos, futTargetPos, targetPos, rotVec;
    public GameObject mantlet, target;
    public GameObject barrel;
    public GameObject bullet;
    public int turretType = 3;
    public List<GameObject> barrels = new List<GameObject>();

    public string turretName = "Default";
    public int barrelCount=1;
    public float caliber=1,length=1;
    public bool isAA = false;
    public bool hasTarget, canFire, mortar;
    public float rotSpeed, horizontalLeft, horizontalRight, highAngle, lowAngle, aimingAngle;
    public float muzzleVelocity = 20;
    public float nextFire=0, fireDelay=5;
    float deltaTimeRef;
    float currentAngle;
    float mantletAngle;
    Vector3 futurePos;
    ObjectPoolController pool;
    public int targeting = 0; //0 - parabolic| 1 - linear | 2 - torpedo
    public int subCount = 0;
    public int turretSize = 0;

    void Awake()
    {
        pool = FindObjectOfType<ObjectPoolController>();
    }

    void Start()
    {
        //setTarget(GameObject.Find("Target"));
        //ChangeBarrels(1);
        foreach (Transform child in transform)
        {
            if (child.tag == "Mantlet")
                mantlet = child.gameObject;
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject clone = Instantiate(barrel, mantlet.transform);
            barrels.Add(clone);
        }
        UpdateTurret();
        GetSubstructureCount();
    }

    public void UpdateTurret()
    {
        ChangeBarrelLength(length);
        ChangeBarrels(barrelCount);
    }

    public void ChangeBarrels(int i)
    {
        for(int x=0;x<4;x++)
        {
            barrels[x].SetActive(false);
        }
        switch(i)
        {
            case 1:
                barrels[0].SetActive(true);
                barrels[0].transform.localPosition = new Vector3(0, 0, 0);
                break;
            case 2:
                barrels[0].SetActive(true);
                barrels[1].SetActive(true);
                barrels[0].transform.localPosition = new Vector3(0.4f+turretType/3/10, 0, 0);
                barrels[1].transform.localPosition = new Vector3(-1*(0.4f + turretType / 3/10), 0, 0);
                break;
            case 3:
                barrels[0].SetActive(true);
                barrels[1].SetActive(true);
                barrels[2].SetActive(true);
                barrels[0].transform.localPosition = new Vector3(0, 0, 0);
                barrels[1].transform.localPosition = new Vector3(0.45f + turretType / 3/10, 0,0);
                barrels[2].transform.localPosition = new Vector3(-1 * (0.45f + turretType / 3/10), 0, 0);
                break;
            case 4:
                barrels[0].SetActive(true);
                barrels[1].SetActive(true);
                barrels[2].SetActive(true);
                barrels[3].SetActive(true);
                barrels[0].transform.localPosition = new Vector3(0.25f + turretType / 3/10, 0, 0);
                barrels[1].transform.localPosition = new Vector3(-1 * (0.25f + turretType / 3/10), 0, 0);
                barrels[2].transform.localPosition = new Vector3(0.55f + turretType / 3/10, 0, 0);
                barrels[3].transform.localPosition = new Vector3(-1 * (0.55f + turretType / 3/10), 0, 0);
                break;
            default:
                Debug.Log("Something broke in turret");
                break;
        }
    }

    public void ChangeBarrelLength(float i)
    {
        for(int x=0;x<barrels.Count;x++)
        {
            barrels[x].transform.localScale = new Vector3(1, 1, i / 24+0.1f);
        }
    }

    RaycastHit hit;
    int placedMask = 1 << 8;
    public void GetSubstructureCount() //Use sub count to determine reload time
    {
        subCount = 0;
        if(Physics.Raycast(transform.position,Vector3.down,out hit,1, placedMask))
        {
            if(hit.transform.GetComponent<Block>().isTurretSubstructure && turretSize == hit.transform.GetComponent<Block>().subSize)
            {
                subCount++;
                while(hit.transform.GetComponent<Block>() && turretSize == hit.transform.GetComponent<Block>().subSize)
                {
                    if (Physics.Raycast(hit.transform.position, Vector3.down, out hit, 1, placedMask))
                    {
                        subCount++;
                    }
                }
            }
        }
    }

    public void SetTarget(GameObject Target)
    {
        target = Target;
        hasTarget = true;
    }

    Vector3 GetFuturePos(float timeOfFlight)
    {
        futTargetPos = (target.transform.position + timeOfFlight * target.GetComponent<Vehicle>().velocity) - transform.position;
        return futTargetPos;
    }

    float GetTargetingAngle()
    {
        float xzDist, thetaHigh, thetaLow, time;

        invTargetPos = target.transform.position - transform.position;
        xzDist = Mathf.Sqrt((invTargetPos.x * invTargetPos.x) + (invTargetPos.z * invTargetPos.z));

        thetaHigh = Mathf.Atan2((muzzleVelocity * muzzleVelocity )+ Mathf.Sqrt((muzzleVelocity* muzzleVelocity* muzzleVelocity* muzzleVelocity) - GlobalVars.gravity * (GlobalVars.gravity * (xzDist * xzDist) + 2 * invTargetPos.y * (muzzleVelocity * muzzleVelocity))),(GlobalVars.gravity * xzDist));
        thetaLow = Mathf.Atan2(((muzzleVelocity* muzzleVelocity) - Mathf.Sqrt((muzzleVelocity* muzzleVelocity* muzzleVelocity* muzzleVelocity) - GlobalVars.gravity * (GlobalVars.gravity * (xzDist* xzDist) + 2 * invTargetPos.y * (muzzleVelocity* muzzleVelocity)))),(GlobalVars.gravity * xzDist));

        if (float.IsNaN(thetaHigh))
        {
            canFire = false;
            return 0;
        }
        if (mortar)
            time = xzDist / (muzzleVelocity * Mathf.Cos(thetaHigh));
        else
            time = xzDist / (muzzleVelocity * Mathf.Cos(thetaLow));

        GetFuturePos(time);
        xzDist = Mathf.Sqrt((futTargetPos.x * futTargetPos.x) + (futTargetPos.z * futTargetPos.z));

        thetaHigh = Mathf.Rad2Deg * Mathf.Atan2((muzzleVelocity * muzzleVelocity) + Mathf.Sqrt((muzzleVelocity * muzzleVelocity * muzzleVelocity * muzzleVelocity) - GlobalVars.gravity * (GlobalVars.gravity * (xzDist * xzDist) + 2 * invTargetPos.y * (muzzleVelocity * muzzleVelocity))), (GlobalVars.gravity * xzDist));
        thetaLow = Mathf.Rad2Deg * Mathf.Atan2(((muzzleVelocity * muzzleVelocity) - Mathf.Sqrt((muzzleVelocity * muzzleVelocity * muzzleVelocity * muzzleVelocity) - GlobalVars.gravity * (GlobalVars.gravity * (xzDist * xzDist) + 2 * invTargetPos.y * (muzzleVelocity * muzzleVelocity)))), (GlobalVars.gravity * xzDist));

        if (float.IsNaN(thetaHigh))
        {
            canFire = false;
            return 0;
        }

        if (mortar)
            return thetaHigh;
        else
            return thetaLow;
    }

    // Update is called once per frame
    void Update()
    {
        deltaTimeRef = Time.deltaTime;


        if (hasTarget)
        {
            aimingAngle = GetTargetingAngle(); //Get the targeting angle 
            futurePos = transform.InverseTransformPoint(futTargetPos + transform.position);
            if (Mathf.Abs(futurePos.x) < 1)
                transform.Rotate(Vector3.up * rotSpeed / 10 * Mathf.Sign(futurePos.x) * deltaTimeRef);
            else
                transform.Rotate(Vector3.up * rotSpeed * Mathf.Sign(futurePos.x) * deltaTimeRef);

            rotVec = transform.localEulerAngles; //Clamp the barrel
            if (rotVec.y < 360 - horizontalLeft && rotVec.y > 180) //Negative numbers wrap to 360 degrees which causes major issues
                rotVec.y = 360 - horizontalLeft;
            if (rotVec.y > Mathf.Abs(horizontalRight) && rotVec.y < 180) //So calculate clamp with 2 seperate limits instead
                rotVec.y = Mathf.Abs(horizontalRight);
            transform.localRotation = Quaternion.Euler(rotVec); //Set clamp
            mantletAngle = Vector3.Angle(Vector3.ProjectOnPlane(mantlet.transform.forward, mantlet.transform.right), Vector3.up) - 90;
            currentAngle = aimingAngle + mantletAngle;
            mantlet.transform.localRotation = Quaternion.Euler(mantlet.transform.localRotation.eulerAngles.x, 0.0f, 0.0f);
            if (Mathf.Abs(currentAngle) < 1)
                mantlet.transform.Rotate((-Vector3.right * rotSpeed / 100 * Mathf.Sign(currentAngle)) * deltaTimeRef);
            else
                mantlet.transform.Rotate((-Vector3.right * rotSpeed / 10 * Mathf.Sign(currentAngle)) * deltaTimeRef);

            rotVec = mantlet.transform.localEulerAngles;

            if (rotVec.x < 360 - highAngle && rotVec.x > 180)
                rotVec.x = 360 - highAngle;
            if (rotVec.x > Mathf.Abs(lowAngle) && rotVec.x < 180)
                rotVec.x = Mathf.Abs(lowAngle);

            mantlet.transform.localRotation = Quaternion.Euler(rotVec);

            if (Time.time > nextFire && Mathf.Abs(currentAngle)<1) //take into account angle from target (not tested!)
            {
                nextFire = Time.time + fireDelay;
                for(int i=0;i<barrelCount;i++)
                {
                    GameObject clone = pool.Spawn("Bullet"); //Temporary (use pool controller)
                    clone.transform.position = barrels[i].transform.GetChild(0).transform.position;
                    clone.transform.rotation = barrels[i].transform.rotation;
                    clone.transform.Rotate(new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f))); //inaccuracy
                    clone.GetComponent<Bullet>().Velocity = muzzleVelocity * clone.transform.forward;
                    clone.SetActive(true);
                    clone = pool.Spawn("fxCannonShot");
                    clone.transform.position = barrels[i].transform.GetChild(0).transform.position;
                    clone.transform.rotation = barrels[i].transform.rotation;
                    clone.SetActive(true);
                }
            }
        }
    }
}
