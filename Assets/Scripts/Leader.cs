﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leader
{
    public int level=1,exp=0;
    public string name = "John Doe";
    public int gender = 0; //0: male 1: female
    public List<int> skills = new List<int>();
    public int leadership = 0, accuracy = 0, damCon = 0, aa = 0;
};
